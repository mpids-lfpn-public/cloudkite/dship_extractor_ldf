#!/usr/bin/env python3

# Copyright 2020-2021 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


""" DSHIP extractor for the research ships of the LDF.

Log extractor and combiner for DSHIP data from research ships of the
Leitstelle Deutsche Forschungsschiffe.


Version 0.4.1

"""

import argparse
import bisect
import collections.abc
import copy
import datetime
import fileinput
import io
import itertools
import json
import logging
import math
import os
import os.path
import platform
import random
import sys
import tarfile
import time
import traceback
import zipfile


import numpy
import scipy
import scipy.constants

import astropy
import astropy.utils.iers

import netCDF4


__version__ = '0.4.1'


# A table is needed to go between DSHIP source names and the quantity
# they are. The source names or shortened forms will be the keys and the
# values will be False if it should not be recorded, True if it should
# be recorded but the name should be that of the source, or a str if it
# should have a different name.
#
# The different source name forms are
#
# 1. The unmodified source name
# 2. The source name with all characters before the first period
#    removed. This is 'CNAV1.GPGGA.Latitude' -> '.GPGGA.Latitude'.
# 3. The same name as from kind 2 but with the period and the first
#    two characters afterwards replaced with '*' as long as those two
#    characters are capital letters.
_src_lookup = {
    '*DPT.Depth': 'sea_floor_depth_below_transducer',
    '*DPT.DepthWl': 'sea_floor_depth_below_sea_surface',
    '*DPT.Offset': 'depth_transducer',
    '*DPT.depth': 'sea_floor_depth_below_sea_surface',
    '*DPT.offset': 'depth_transducer',
    '*DBS.DepthFathoms': 'sea_floor_depth_below_sea_surface',
    '*DBS.DepthFeet': 'sea_floor_depth_below_sea_surface',
    '*DBS.DepthMeter': 'sea_floor_depth_below_sea_surface',
    '*GGA.Age': 'dgps_differential_age',
    '*GGA.Altitude': 'alt',
    '*GGA.Dilution': 'horizontal_position_dilution_of_precision',
    '*GGA.EW': False,
    '*GGA.Latitude': 'lat',
    '*GGA.Longitude': 'lon',
    '*GGA.NS': False,
    '*GGA.Quality': 'gnss_quality',
    '*GGA.Satellites': 'number_satellites_in_view',
    '*GGA.Separation': 'geoid_height_above_reference_ellipsoid',
    '*GGA.Station_ID': 'dgps_base_station_id',
    '*GGA.UTC': False,
    '*GLL.EW': False,
    '*GLL.Latitude': 'lat',
    '*GLL.Longitude': 'lon',
    '*GLL.Mode': True,
    '*GLL.Mode_indicator': True,
    '*GLL.NS': False,
    '*GLL.UTC': False,
    '*HDM.heading_magnetic': 'platform_yaw_fore_starboard_magnetic',
    '*HDT.Degrees': False,
    '*HDT.Heading': 'platform_yaw_fore_starboard',
    '*HDT.HeadingT': 'platform_yaw_fore_starboard',
    '*HDT.heading': 'platform_yaw_fore_starboard',
    '*VBW.LongitudinalGroundSpeed': 'platform_fore_velocity_wrt_ground',
    '*VBW.LongitudinalSpeed': True,
    '*VBW.LongitudinalWaterSpeed': 'platform_fore_velocity_wrt_sea_water',
    '*VBW.TransverseGroundSpeed': 'platform_starboard_velocity_wrt_ground',
    '*VBW.TransverseSpeed': True,
    '*VBW.TransverseWaterSpeed': 'platform_starboard_velocity_wrt_sea_water',
    '*VHW.LongitudinalWaterSpeed': 'platform_fore_velocity_wrt_sea_water',
    '*VTG.CourseM': 'platform_course_magnetic',
    '*VTG.CourseT': 'platform_course',
    '*VTG.Mode': True,
    '*VTG.SpeedK': 'platform_speed_wrt_ground',
    '*VTG.SpeedN': 'platform_speed_wrt_ground',
    '*ZDA.Day': False,
    '*ZDA.Hours': False,
    '*ZDA.Minutes': False,
    '*ZDA.Month': False,
    '*ZDA.UTC': False,
    '*ZDA.Year': False,
    '.CMEAN.Soundvelocity': 'speed_of_sound_in_sea_water',
    '.SBE21.Conductivity': 'sea_water_electrical_conductivity',
    '.SBE21.Conductivity_mS': 'sea_water_electrical_conductivity',
    '.SBE21.Density': 'sea_water_density',
    '.SBE21.Salinity': 'sea_water_salinity',
    '.SBE21.SoundVeloExt': 'speed_of_sound_in_sea_water',
    '.SBE21.TempExtern': 'sea_surface_temperature',
    '.PPPRP.pitch': 'platform_pitch_fore_up',
    '.PPPRP.roll': 'platform_roll_starboard_down',
    '.PSXN.Heading': 'platform_yaw_fore_starboard',
    '.PSXN.Heave': 'platform_heave_down',
    '.PSXN.ID': True,
    '.PSXN.Pitch': 'platform_pitch_fore_up',
    '.PSXN.Roll': 'platform_roll_starboard_down',
    '.SVT.Soundvelocity': 'speed_of_sound_in_sea_water',
    '.TIROT.Turn': 'platform_yaw_rate_fore_starboard',
    '.svt.Soundvelocity': 'speed_of_sound_in_sea_water',
    'EA600.D1.Depth': 'sea_floor_depth_below_sea_surface',
    'EA600.D1.Frequence': True,
    'EA600.D1.Reflectivity': 'sea_floor_sound_reflectivity',
    'EA600.D1.Tranducer': True,
    'EA600.D2.Depth': 'sea_floor_depth_below_sea_surface',
    'EA600.D2.Frequence': True,
    'EA600.D2.Reflectivity': 'sea_floor_sound_reflectivity',
    'EA600.D2.Tranducer': True,
    'EA600.D3.Depth': 'sea_floor_depth_below_sea_surface',
    'EA600.D3.Frequence': True,
    'EA600.D3.Reflectivity': 'sea_floor_sound_reflectivity',
    'EA600.D3.Tranducer': True,
    'Global_radiation.SMSM.GS': 'surface_downwelling_shortwave_flux_in_air',
    'Global_radiation.SMSM.IR': 'surface_downwelling_longwave_flux_in_air',
    'Global_radiation.SMSMN.GS': 'surface_downwelling_shortwave_flux_in_air',
    'Global_radiation.SMSMN.IR': 'surface_downwelling_longwave_flux_in_air',
    'Global_radiation.SMSMN.PA': True,
    'Global_radiation.SMSMN.TI': 'instrument_temperature_radiation_sensor',
    'NACOS.RAOSD.CourseReference': False,
    'NACOS.RAOSD.CourseT': False,
    'NACOS.RAOSD.HeadingT': False,
    'NACOS.RAOSD.Speed': False,
    'NACOS.RAOSD.SpeedReference': False,
    'NACOS.RAOSD.SpeedUnits': False,
    'NACOS.RAOSD.Status': False,
    'NACOS.RAOSD.VesselDrift': False,
    'NACOS.RAOSD.VesselSetT': False,
    'NACOS.RARTE.MessageMode': False,
    'NACOS.RARTE.MessageNumber': False,
    'NACOS.RARTE.RouteId': False,
    'NACOS.RARTE.WaypointIdFrom': False,
    'NACOS.RARTE.WaypointIdTo': False,
    'NACOS.RAWPL.DirectionEW': False,
    'NACOS.RAWPL.DirectionNS': False,
    'NACOS.RAWPL.Identifier': False,
    'NACOS.RAWPL.Latitude': False,
    'NACOS.RAWPL.Longitude': False,
    'NavLot.PSKPDPT.Offset_from_transducer': 'depth_transducer',
    'NavLot.PSKPDPT.Water_depth_relative': 'sea_floor_depth_below_transducer',
    'Parasound.PHDPT.offset_from_transducer_PHF': 'depth_transducer',
    'Parasound.PHDPT.water_depth_PHF': 'sea_floor_depth_below_transducer',
    'Parasound.PLDPT.offset_from_transducer_SLF_PLF': 'depth_transducer',
    'Parasound.PLDPT.water_depth_SLF_PLF': 'sea_floor_depth_below_transducer',
    'RSWS.RSSMC.SMCS': True,
    'RSWS.RSSMC.SMCtr': True,
    'Rainmeter.RAINX.Sensor1': True,
    'Rainmeter.RAINX.Sensor2': True,
    'SYS.CALC.AbsoluteWind_kn': 'wind_speed',
    'SYS.CALC.BearingNextWaypoint': False,
    'SYS.CALC.Dist2Go': False,
    'SYS.CALC.ETA': False,
    'SYS.CALC.LongitudinalWaterSpeedKmH': 'platform_fore_velocity_wrt_sea_water',
    'SYS.CALC.LongitudinalWaterSpeedM': 'platform_fore_velocity_wrt_sea_water',
    'SYS.CALC.MaxSpeedN': False,
    'SYS.CALC.NextWP': False,
    'SYS.CALC.NextWPLat': False,
    'SYS.CALC.NextWPLon': False,
    'SYS.CALC.POS_EW': False,
    'SYS.CALC.POS_LAT_gg_mmN': False,
    'SYS.CALC.POS_LAT_ggmm': False,
    'SYS.CALC.POS_LAT_radiant': False,
    'SYS.CALC.POS_LON_dez_unsigned': False,
    'SYS.CALC.POS_LON_gggmm': False,
    'SYS.CALC.POS_LON_radiant': False,
    'SYS.CALC.POS_NS': False,
    'SYS.CALC.RelativeWind_kn': 'platform_speed_wrt_air',
    'SYS.CALC.SPEED_kmh': 'platform_speed_wrt_ground',
    'SYS.CALC.Time2Go': False,
    'SYS.CALC.Time_NMEA': False,
    'SYS.CALC.Timestamp': False,
    'SYS.CALC.TransversWaterSpeedM': 'platform_starboard_velocity_wrt_sea_water',
    'SYS.CALC.date_ddmmyy': False,
    'SYS.CALC.date_ddmmyyyy': False,
    'SYS.CALC.time_hhmmss': False,
    'SYS.DISP.ActName': False,
    'SYS.DISP.ExpName': False,
    'SYS.DISP.ExpNumber': False,
    'SYS.DISP.PosLatTxt': False,
    'SYS.DISP.PosLonTxt': False,
    'SYS.DISP.Position': False,
    'SYS.STR.Course': 'platform_course',
    'SYS.STR.Course_O': True,
    'SYS.STR.DPT': 'sea_floor_depth_below_sea_surface',
    'SYS.STR.DPT_O': True,
    'SYS.STR.DateDay': False,
    'SYS.STR.DateDay_O': False,
    'SYS.STR.DateMonth': False,
    'SYS.STR.DateMonth_O': False,
    'SYS.STR.DateYear': False,
    'SYS.STR.DateYear_O': False,
    'SYS.STR.HDG': 'platform_yaw_fore_starboard_magnetic',
    'SYS.STR.HDG_O': True,
    'SYS.STR.PosLat': 'lat',
    'SYS.STR.PosLat_O': True,
    'SYS.STR.PosLon': 'lon',
    'SYS.STR.PosLon_O': True,
    'SYS.STR.SoundVelocity': 'speed_of_sound_in_sea_water',
    'SYS.STR.SoundVelocity_O': True,
    'SYS.STR.Speed': 'platform_speed_wrt_ground',
    'SYS.STR.Speed_O': True,
    'SYS.STR.TimeInSec': False,
    'SYS.STR.TimeInSec_O': False,
    'WEATHER.PBWWI.AirPress': 'air_pressure',
    'WEATHER.PBWWI.AirTempPort': 'air_temperature',
    'WEATHER.PBWWI.AirTempStarboard': 'air_temperature',
    'WEATHER.PBWWI.DewPointPort': 'dew_point_temperature',
    'WEATHER.PBWWI.DewPointStarboard': 'dew_point_temperature',
    'WEATHER.PBWWI.GlobalRadiation': 'surface_downwelling_shortwave_flux_in_air',
    'WEATHER.PBWWI.HumidityPort': 'relative_humidity',
    'WEATHER.PBWWI.HumidityStarboard': 'relative_humidity',
    'WEATHER.PBWWI.LwRadiation': 'surface_downwelling_longwave_flux_in_air',
    'WEATHER.PBWWI.PyrradiometerTemp': 'instrument_temperature_radiation_sensor',
    'WEATHER.PBWWI.RealWindDir': 'wind_to_direction_wrt_platform',
    'WEATHER.PBWWI.RelWindSpeed': 'platform_speed_wrt_air',
    'WEATHER.PBWWI.TrueWindDir': 'wind_to_direction',
    'WEATHER.PBWWI.TrueWindSpeed': 'wind_speed',
    'WEATHER.PBWWI.UvRadiation': 'surface_downwelling_ultraviolet_flux_in_air',
    'WEATHER.PBWWI.Visibility': 'visibility_in_air',
    'WEATHER.PBWWI.WaterTempPort': 'sea_surface_temperature',
    'WEATHER.PBWWI.WaterTempStarboard': 'sea_surface_temperature',
    'Wamos.PWAM.Current_Direction': 'sea_water_velocity_to_direction',
    'Wamos.PWAM.Current_Speed': 'sea_water_speed',
    'Wamos.PWAM.Mean_Period': 'sea_surface_swell_wave_mean_period',
    'Wamos.PWAM.Peak_Wave_Direction': 'sea_surface_swell_wave_to_direction',
    'Wamos.PWAM.Peak_Wave_Direction_Second': 'sea_surface_primary_swell_wave_to_direction',
    'Wamos.PWAM.Peak_Wave_Direction_first': 'sea_surface_secondary_swell_wave_to_direction',
    'Wamos.PWAM.Peak_Wave_Length': 'sea_surface_swell_wave_length',
    'Wamos.PWAM.Peak_Wave_Length_first': 'sea_surface_primary_swell_wave_length',
    'Wamos.PWAM.Peak_Wave_Length_second': 'sea_surface_secondary_swell_wave_length',
    'Wamos.PWAM.Peak_Wave_Period': 'sea_surface_swell_wave_period',
    'Wamos.PWAM.Peak_Wave_Period_Second': 'sea_surface_primary_swell_wave_period',
    'Wamos.PWAM.Peak_Wave_Period_first': 'sea_surface_secondary_swell_wave_period',
    'Wamos.PWAM.Significant_Wave_Height': 'sea_surface_swell_wave_significant_height',
    'Weatherstation.PDWDA.Absolute_wind_direction': 'wind_to_direction',
    'Weatherstation.PDWDA.Absolute_wind_speed_ms': 'wind_speed',
    'Weatherstation.PDWDA.Air_pressure': 'air_pressure',
    'Weatherstation.PDWDA.Air_temperature': 'air_temperature',
    'Weatherstation.PDWDA.Humidity': 'relative_humidity',
    'Weatherstation.PDWDA.Relative_wind_direction': 'wind_to_direction_wrt_platform',
    'Weatherstation.PDWDA.Relative_wind_speed_ms': 'platform_speed_wrt_air',
    'Weatherstation.PDWDA.Water_temperature': 'sea_surface_temperature',
    'Weatherstation.PEUMA.Absolute_wind_direction': 'wind_to_direction',
    'Weatherstation.PEUMA.Absolute_wind_speed': 'wind_speed',
    'Weatherstation.PEUMA.Absolute_wind_speed_bf': 'beaufort_wind_force',
    'Weatherstation.PEUMA.Air_pressure': 'air_pressure',
    'Weatherstation.PEUMA.Air_pressure2': 'air_pressure_at_mean_sea_level',
    'Weatherstation.PEUMA.Air_temperature': 'air_temperature',
    'Weatherstation.PEUMA.Humidity': 'relative_humidity',
    'Weatherstation.PEUMA.Relative_wind_direction': 'wind_to_direction_wrt_platform',
    'Weatherstation.PEUMA.Relative_wind_speed': 'platform_speed_wrt_air',
    'Weatherstation.PEUMA.Relative_wind_speed_bf': 'beaufort_wind_force_wrt_platform',
    'Weatherstation.PEUMA.Water_temperature': 'sea_surface_temperature',
    'Wempe.wempe.DateLZ': False,
    'Wempe.wempe.DateUTC': False,
    'Wempe.wempe.TimeLZ': False,
    'Wempe.wempe.TimeUTC': False,
    'Wempe.wempe.Wempe': False}

# Another table is needed to look up each quantity name to get its dtype
# and Attributes. Each value will be a tuple of the dtype and a dict of
# the Attributes.
_quantity_lookup = {
    '*VTG.Mode': (
        'S1',
        {}),
    '*VTG.Mode': (
        'S1',
        {}),
    '*GLL.Mode': (
        'S1',
        {}),
    '*GLL.Mode_indicator': (
        'S1',
        {}),
    '*VBW.LongitudinalSpeed': (
        'float32',
        {'units': 'm/s'}),
    '*VBW.TransverseSpeed': (
        'float32',
        {'units': 'm/s'}),
    '.PSXN.ID': (
        'int32',
        {}),
    'EA600.D1.Frequence': (
        'float32',
        {}),
    'EA600.D1.Tranducer': (
        'float32',
        {}),
    'EA600.D2.Frequence': (
        'float32',
        {}),
    'EA600.D2.Tranducer': (
        'float32',
        {}),
    'EA600.D3.Frequence': (
        'float32',
        {}),
    'EA600.D3.Tranducer': (
        'float32',
        {}),
    'RSWS.RSSMC.SMCS': (
        'float32',
        {}),
    'RSWS.RSSMC.SMCtr': (
        'float32',
        {}),
    'Rainmeter.RAINX.Sensor1': (
        'float32',
        {}),
    'Rainmeter.RAINX.Sensor2': (
        'float32',
        {}),
    'SYS.STR.Course_O': (
        str,
        {}),
    'SYS.STR.DPT_O': (
        str,
        {}),
    'SYS.STR.HDG_O': (
        str,
        {}),
    'SYS.STR.PosLat_O': (
        str,
        {}),
    'SYS.STR.PosLon_O': (
        str,
        {}),
    'SYS.STR.SoundVelocity_O': (
        str,
        {}),
    'SYS.STR.Speed_O': (
        str,
        {}),
    'air_pressure': (
        'float32',
        {'standard_name': 'air_pressure',
         'units': 'Pa',
         'valid_min': numpy.float32(0)}),
    'air_pressure_at_mean_sea_level': (
        'float32',
        {'standard_name': 'air_pressure_at_mean_sea_level',
         'units': 'Pa',
         'valid_min': numpy.float32(0)}),
    'air_temperature': (
        'float32',
        {'standard_name': 'air_temperature',
         'units': 'K',
         'valid_min': numpy.float32(0)}),
    'alt': (
        'float32',
        {'standard_name': 'altitude',
         'units': 'm',
         'positive': 'up',
         'axis': 'Z'}),
    'beaufort_wind_force': (
        'float32',
        {'standard_name': 'beaufort_wind_force',
         'units': '1',
         'valid_min': numpy.float32(0)}),
    'beaufort_wind_force_wrt_platform': (
        'float32',
        {'units': '1',
         'valid_min': numpy.float32(0)}),
    'depth_transducer': (
        'float32',
        {'units': 'm'}),
    'dew_point_temperature': (
        'float32',
        {'standard_name': 'dew_point_temperature',
         'units': 'K',
         'valid_min': numpy.float32(0)}),
    'dgps_base_station_id': (
        'int32',
        {}),
    'dgps_differential_age': (
        'float32',
        {'units': 's'}),
    'geoid_height_above_reference_ellipsoid': (
        'float32',
        {'standard_name': 'geoid_height_above_reference_ellipsoid',
         'units': 'm',
         'comment':
         'Altitude difference in earth models, which is '
         'ellipsoidal-altitude (WGS-84-altitude, presumably) - '
         'MSL-altitude (geoid).',
         'grid_mapping': 'crs'}),
    'gnss_quality': (
        'int8',
        {}),
    'horizontal_position_dilution_of_precision': (
        'float32',
        {'valid_min': numpy.float32(0)}),
    'instrument_temperature_radiation_sensor': (
        'float32',
        {'valid_min': numpy.float32(0)}),
    'lat': (
        'float64',
        {'standard_name': 'latitude',
         'units': 'degree_north',
         'axis': 'X'}),
    'lon': (
        'float64',
        {'standard_name': 'longitude',
         'units': 'degree_east',
         'axis': 'Y'}),
    'number_satellites_in_view': (
        'int8',
        {}),
    'platform_course': (
        'float32',
        {'standard_name': 'platform_course',
         'units': 'degree',
         'valid_range': numpy.float32((0, 360))}),
    'platform_course_magnetic': (
        'float32',
        {'units': 'degree',
         'valid_range': numpy.float32((0, 360))}),
    'platform_fore_velocity_wrt_ground': (
        'float32',
        {'units': 'm/s'}),
    'platform_fore_velocity_wrt_sea_water': (
        'float32',
        {'units': 'm/s'}),
    'platform_heave_down': (
        'float32',
        {'standard_name': 'platform_heave_down',
         'units': 'm'}),
    'platform_pitch_fore_up': (
        'float32',
        {'standard_name': 'platform_pitch_fore_up',
         'units': 'degree',
         'valid_range': numpy.float32((-90, 90))}),
    'platform_roll_starboard_down': (
        'float32',
        {'standard_name': 'platform_roll_starboard_down',
         'units': 'degree',
         'valid_range': numpy.float32((-180, 180))}),
    'platform_speed_wrt_air': (
        'float32',
        {'standard_name': 'platform_speed_wrt_air',
         'units': 'm/s',
         'valid_min': numpy.float32(0)}),
    'platform_speed_wrt_ground': (
        'float32',
        {'standard_name': 'platform_speed_wrt_ground',
         'units': 'm/s',
         'valid_min': numpy.float32(0)}),
    'platform_starboard_velocity_wrt_ground': (
        'float32',
        {'units': 'm/s'}),
    'platform_starboard_velocity_wrt_sea_water': (
        'float32',
        {'units': 'm/s'}),
    'platform_yaw_fore_starboard': (
        'float32',
        {'standard_name': 'platform_yaw_fore_starboard',
         'units': 'degree',
         'valid_range': numpy.float32((0, 360))}),
    'platform_yaw_fore_starboard_magnetic': (
        'float32',
        {'units': 'degree',
         'valid_range': numpy.float32((0, 360))}),
    'platform_yaw_rate_fore_starboard': (
        'float32',
        {'standard_name': 'platform_yaw_rate_fore_starboard',
         'units': 'degree/s'}),
    'relative_humidity': (
        'float32',
        {'standard_name': 'relative_humidity',
         'units': '1',
         'valid_min': numpy.float32(0)}),
    'sea_floor_depth_below_sea_surface': (
        'float32',
        {'standard_name': 'sea_floor_depth_below_sea_surface',
         'units': 'm',
         'valid_min': numpy.float32(0)}),
    'sea_floor_depth_below_transducer': (
        'float32',
        {'units': 'm',
         'valid_min': numpy.float32(0)}),
    'sea_floor_sound_reflectivity': (
        'float32',
        {'valid_min': numpy.float32(0)}),
    'sea_surface_primary_swell_wave_length': (
        'float32',
        {'units': 'm'}),
    'sea_surface_primary_swell_wave_period': (
        'float32',
        {'units': 's'}),
    'sea_surface_primary_swell_wave_to_direction': (
        'float32',
        {'units': 'degree',
         'valid_range': numpy.float32((0, 360))}),
    'sea_surface_secondary_swell_wave_length': (
        'float32',
        {'units': 'm'}),
    'sea_surface_secondary_swell_wave_period': (
        'float32',
        {'units': 's'}),
    'sea_surface_secondary_swell_wave_to_direction': (
        'float32',
        {'units': 'degree',
         'valid_range': numpy.float32((0, 360))}),
    'sea_surface_swell_wave_length': (
        'float32',
        {'units': 'm'}),
    'sea_surface_swell_wave_mean_period': (
        'float32',
        {'standard_name': 'sea_surface_swell_wave_mean_period',
         'units': 's'}),
    'sea_surface_swell_wave_period': (
        'float32',
        {'standard_name': 'sea_surface_swell_wave_period',
         'units': 's'}),
    'sea_surface_swell_wave_significant_height': (
        'float32',
        {'standard_name': 'sea_surface_swell_wave_significant_height',
         'units': 'm',
         'valid_min': numpy.float32(0)}),
    'sea_surface_swell_wave_to_direction': (
        'float32',
        {'standard_name': 'sea_surface_swell_wave_to_direction',
         'units': 'degree',
         'valid_range': numpy.float32((0, 360))}),
    'sea_surface_temperature': (
        'float32',
        {'standard_name': 'sea_surface_temperature',
         'units': 'K',
         'valid_min': numpy.float32(0)}),
    'sea_water_density': (
        'float32',
        {'standard_name': 'sea_water_density',
         'units': 'kg/m^3',
         'valid_min': numpy.float32(0)}),
    'sea_water_electrical_conductivity': (
        'float32',
        {'standard_name': 'sea_water_electrical_conductivity',
         'units': 'S/m',
         'valid_min': numpy.float32(0)}),
    'sea_water_salinity': (
        'float32',
        {'standard_name': 'sea_water_salinity',
         'units': '1e-3',
         'valid_min': numpy.float32(0)}),
    'sea_water_speed': (
        'float32',
        {'standard_name': 'sea_water_speed',
         'units': 'm/s',
         'valid_min': numpy.float32(0)}),
    'sea_water_velocity_to_direction': (
        'float32',
        {'standard_name': 'sea_water_velocity_to_direction',
         'units': 'degree',
         'valid_range': numpy.float32((0, 360))}),
    'speed_of_sound_in_sea_water': (
        'float32',
        {'standard_name': 'speed_of_sound_in_sea_water',
         'units': 'm/s',
         'valid_min': numpy.float32(0)}),
    'surface_downwelling_longwave_flux_in_air': (
        'float32',
        {'standard_name': 'surface_downwelling_longwave_flux_in_air',
         'units': 'W/m^2',
         'valid_min': numpy.float32(0)}),
    'Global_radiation.SMSMN.PA': (str, dict()),
    'surface_downwelling_shortwave_flux_in_air': (
        'float32',
        {'standard_name': 'surface_downwelling_shortwave_flux_in_air',
         'units': 'W/m^2',
         'valid_min': numpy.float32(0)}),
    'surface_downwelling_ultraviolet_flux_in_air': (
        'float32',
        {'units': 'W/m^2',
         'valid_min': numpy.float32(0)}),
    'visibility_in_air': (
        'float32',
        {'standard_name': 'visibility_in_air',
         'units': 'm',
         'valid_min': numpy.float32(0)}),
    'wind_speed': (
        'float32',
        {'standard_name': 'wind_speed',
         'units': 'm/s',
         'valid_min': numpy.float32(0)}),
    'wind_to_direction': (
        'float32',
        {'standard_name': 'wind_to_direction',
         'units': 'degree',
         'valid_range': numpy.float32((0, 360))}),
    'wind_to_direction_wrt_platform': (
        'float32',
        {'units': 'degree',
         'valid_range': numpy.float32((0, 360))})
    }


# Function that finds the name to use in _src_lookup.
def _find_name_in_src_lookup(name):
    """ Find the name to use in the lookup.

    Parameters
    ----------
    name : str
        The name to lookup.

    Returns
    -------
    lookup_name : str or None
        The name to use in the lookup or ``None`` if it isn't in the
        lookup at all.

    """
    if name in _src_lookup:
        return name
    parts = name.split('.')
    if len(parts) < 2:
        return None
    lookup_name = '.' + '.'.join(parts[1:])
    if lookup_name in _src_lookup:
        return lookup_name
    if len(lookup_name) <= 3 or not lookup_name[1:3].isupper():
        return None
    lookup_name = '*' + lookup_name[3:]
    if lookup_name in _src_lookup:
        return lookup_name
    else:
        return None


def process_dship_logs(output_path, paths, chunksize=100000,
                       log_to_stdout=True, log_to_file=False, **attrs):
    """ Processes and combines DSHIP logs into a single NetCDF4 file.

    Takes one or more DSHIP logs from one of the research ships of the
    Leitstelle Deutsche Forschungsschiffe (LDF), extracts and combines
    the contents, and stores the results in a single NetCDF4 file. The
    processing is two pass in order to get the precisions reliably in
    the case that the logs are storing more digits than the values
    need.

    Parameters
    ----------
    output_path : str
        The path for the output file, which will be a NetCDF4 (means it
        is also an HDF5 file). Any existing file will be truncated. Home
        variable expansion is performed. No file will be created if
        `paths` is empty.
    paths : str or Iterable
        The path/s to the DSHIP files to process, or directories to
        recursively search for DSHIP files to process under. Can be a
        single path or more than one packed into an Iterable. Home
        variable expansion is done on each path. When searching a
        directory recursively, it is done in top-down order with the
        file and directory names sorted and files are included if they
        end in the extensions ``'.dat'``, ``'.dat.gz'``, ``'.dat.bz2'``,
        ``'.zip'``, ``'.tar'``, ``'.tar.gz'``, ``'.tar.bz2'``,
        ``'.tgz'``, or ``'.tbz2'``.
    chunksize : int, optional
        The chunk length to use for storing the variables. Must be at
        least 1000. Default is 100,000; which is a good balance
        between file size reduction, read and write latency, not
        having too many chunks, and the chunks fitting in the chunk
        cache.
    log_to_stdout : bool, optional
        Whether basic information during the processing should be logged
        to stdout or not. Default is ``True``.
    log_to_file : bool, optional
        Whether detailed information during the processing should be
        logged to a file with the same name as `output_path` but with
        the extension changed to ``'.log'``. Default is ``False``.
    **attrs : extra keyword arguments
        The Attributes to set in the file beyond the very basic ones
        that can be determined automatically.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.
    IOError
        If a file can't be found or there is an IO error of some sort.
    RuntimeError
        If the logs are inconsistent with each other.
    NotImplementedError
        If a leap second occurs during the time range of the logs.

    """
    # Check the arguments, do user home expansion and path
    # normalization, and remove duplicates while preserving order.
    if not isinstance(log_to_stdout, bool):
        raise TypeError('log_to_stdout must be bool.')
    if not isinstance(log_to_file, bool):
        raise TypeError('log_to_file must be bool.')
    if not isinstance(output_path, str):
        raise TypeError('output_path must be str.')
    output_path = os.path.normpath(os.path.expanduser(output_path))
    if not isinstance(paths, collections.abc.Iterable):
        raise TypeError('paths must be an Iterable.')
    if not isinstance(chunksize, int):
        raise TypeError('chunksize must be an int.')
    if chunksize < 1000:
        raise ValueError('chunksize must be at least 1000.')
    paths = [v for v in paths]
    for v in paths:
        if not isinstance(v, str):
            raise TypeError('Each element of paths must be str.')
    paths_in_order = []
    for v in paths:
        pth = os.path.normpath(os.path.expanduser(v))
        if pth not in paths_in_order:
            if os.path.isfile(pth):
                paths_in_order.append(pth)
            elif os.path.isdir(pth):
                # It is a directory. It will be searched recursively for
                # files of the right format in a sorted order starting
                # from the top (sorting makes the order deterministic).
                for dirpath, dirnames, filenames in os.walk(
                        pth, topdown=True, followlinks=True):
                    dirnames.sort()
                    filenames.sort()
                    for name in filenames:
                        for k in ('.dat', '.dat.gz', '.dat.bz2',
                                  '.zip', '.tar', '.tar.gz', '.tar.bz2',
                                  '.tgz', '.tbz2'):
                            if name.endswith(k):
                                paths_in_order.append(os.path.join(
                                    dirpath, name))
                                break
            else:
                raise IOError('Could not find a file or directory: '
                              + pth)
    # Return if there is nothing to do.
    if len(paths_in_order) == 0:
        return
    # Add some standard CF Attributes.
    attrs['Conventions'] = 'CF-1.8'
    attrs['standard_name_vocabulary'] = 'CF Standard Name Table v72'
    attrs['source'] = 'ship board instruments via the DSHIP system'
    attrs['software_versions'] = 'dship_extractor_ldf: "{0:s}"\n' \
        'numpy: "{1:s}"\n' \
        'scipy: "{2:s}"\n' \
        'astropy: "{3:s}"\n' \
        'netCDF4: "{4:s}"\n' \
        'python: "{5:s} {6:s}"'.format(__version__, numpy.__version__, \
        scipy.__version__, astropy.__version__, netCDF4.__version__, \
        platform.python_implementation(), platform.python_version())
    attrs['history'] = (
        datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
        + ' Produced by dship_extractor_ldf (version {0}) from '
        'DSHIP log exports.'.format(__version__))
    # Make the arguments that are common to every variable being
    # created.
    vargs = {'zlib': True,
             'complevel': 9,
             'fletcher32': True,
             'contiguous': False}
    # Open all the dship logs and setup the logger to stdout and/or
    # file. No matter what happens, we must close them when we are done.
    logits = []
    logger = None
    try:
        # Make the logger if log_to_stdout or log_to_file are True.
        if log_to_stdout or log_to_file:
            logger = logging.Logger(
                __package__ + '{0:016X}'.format(random.getrandbits(64)),
                level=logging.DEBUG)
        if log_to_stdout:
            logging_stdout_formatter = logging.Formatter(
                fmt='%(asctime)s %(levelname)-8s %(message)s',
                datefmt='%Y-%m-%d %H:%M:%S')
            logging_stdout_formatter.converter = time.gmtime
            logging_stdout_handler = logging.StreamHandler(
                stream=sys.stdout)
            logging_stdout_handler.setLevel(logging.INFO)
            logging_stdout_handler.setFormatter(
                logging_stdout_formatter)
            logger.addHandler(logging_stdout_handler)
        if log_to_file:
            logging_file_formatter = logging.Formatter(
                fmt='%(asctime)s %(levelname)-8s '
                '%(module)-20s %(funcName)-30s %(lineno)-4d '
                '%(message)s',
                datefmt='%Y-%m-%dT%H:%M:%SZ')
            logging_file_formatter.converter = time.gmtime
            logging_file_handler = logging.FileHandler(
                os.path.splitext(output_path)[0] + '.log',
                mode='w', encoding='utf-8')
            logging_file_handler.setLevel(logging.DEBUG)
            logging_file_handler.setFormatter(logging_file_formatter)
            logger.addHandler(logging_file_handler)
        # Log all output files.
        if logger is not None:
            logger.debug('Log paths in order ({0} in total):'.format(
                len(paths_in_order)))
            for v in paths_in_order:
                logger.debug('  ' + v)
        # Make the log iterators. We will be doing two pass, so the
        # first pass will be here which could take a while.
        if logger is not None:
            logger.info('Doing first pass on each log:')
        for i, v in enumerate(paths_in_order):
            logits.append(DshipLogIterator(v, two_pass=True))
            if logger is not None:
                logger.info('  {0} / {1} complete'.format(
                    i, len(paths_in_order)))
        # We need to go through every quantity in the log iterators and
        # determine what variables we will need to make and how.
        #
        # If a quantity name has only a single source name (but possibly
        # more than one log), then nothing special needs to be done and
        # it is a simple 1D variable with the source name being in its
        # dship_source_name Attribute.
        #
        # If a quantity has more than one source name, then we make both
        # a 1D variable of it and a 2D variable of it with a breakdown
        # by source.
        #
        # First, we need to find all quantities, their sources, and
        # whether they are floating point or not (floating point
        # quantities need a precision variable).
        #
        # We will collect the names of all sources used and all rejected
        # ones and one with no valid value, which will then be put into
        # the Dataset Attributes.
        used_sources = set()
        novalid_sources = set()
        rejected_sources = set()
        if logger is not None:
            logger.info('Determining quantities, their information, '
                        'and their relationships.')
        qs_info = dict()
        for i, v in enumerate(logits):
            qnames = v.quantity_names
            for j, (k, tp, anm) in enumerate(zip(v.source_names,
                                                 v.types,
                                                 v.any_non_missing)):
                k_lookup = _find_name_in_src_lookup(k)
                if k_lookup is None or _src_lookup[k_lookup] is False:
                    rejected_sources.add(k)
                elif not anm:
                    novalid_sources.add(k)
                else:
                    used_sources.add(k)
                    qname = qnames[j]
                    if isinstance(tp, str):
                        has_prec = tp.startswith('float')
                    elif isinstance(tp, numpy.dtype):
                        has_prec = numpy.issubclass_(
                            tp.type, numpy.inexact)
                    else:
                        has_prec = False
                    entry = (i, j, k, qname, has_prec)
                    if qname in qs_info:
                        qs_info[qname].append(entry)
                    else:
                        qs_info[qname] = [entry]
        used_sources = list(used_sources)
        novalid_sources = list(novalid_sources)
        rejected_sources = list(rejected_sources)
        used_sources.sort()
        novalid_sources.sort()
        rejected_sources.sort()
        attrs['source_names_used'] = used_sources
        attrs['source_names_with_no_non_missing_data'] = novalid_sources
        attrs['source_names_rejected'] = rejected_sources
        # Log the soure names in each group.
        if logger is not None:
            logger.debug('Found the following source names, arranged '
                         'by how they are handled.')
            for k, desc in (
                    ('source_names_used', 'Using:'),
                    ('source_names_with_no_non_missing_data',
                     'Rejected because all values are missing:'),
                    ('source_names_rejected',
                     'Rejected because the quantity is duplicative '
                     '(date and time) or could not be identified:')):
                logger.debug(2 * ' ' + desc)
                for v in attrs[k]:
                    logger.debug(4 * ' ' + v)
        # Now we need to separate qs_info into two parts, ones that are
        # single source and ones that are multi source. For those with
        # multiple sources, we need to set the columns. For both, we
        # need to determine if a precision variable is required (it is
        # required if but a single source will return the precision).
        qs_single = dict()
        qs_multi = dict()
        for k, v in qs_info.items():
            has_precision = any([v2[-1] for v2 in v])
            if len({k2 for _, _, k2, _, _ in v}) == 1:
                qs_single[k] = {'source': v,
                                'source_name': v[0][2],
                                'has_precision': has_precision}
            else:
                cols = []
                for _, _, k2, _, _ in v:
                    if k2 not in cols:
                        cols.append(k2)
                qs_multi[k] = {'sources': v,
                               'columns': cols,
                               'has_precision': has_precision}
                # If only one column is a SYS quantity, then that will
                # be what we select for the single value.
                sysquant = {v2 for v2 in cols if v2.startswith('SYS.')}
                if len(sysquant) == 1:
                    qs_multi[k]['single_val'] = list(sysquant)[0]
        # Lookup the dtype, missing value, and attributes for each
        # quantity. We also need to determine if we need the WGS 84 grid
        # mapping or not.
        need_wgs84_grid_mapping = False
        for k, v in itertools.chain(qs_single.items(),
                                    qs_multi.items()):
            if k in _quantity_lookup:
                dtype_str, vattrs = _quantity_lookup[k]
            else:
                dtype_str, vattrs = _quantity_lookup[
                    _find_name_in_src_lookup(k)]
            vattrs = copy.deepcopy(vattrs)
            if dtype_str == str:
                dtype = numpy.dtype('object')
                missing_value = ''
            else:
                dtype = numpy.dtype(dtype_str)
                if numpy.issubclass_(dtype.type, numpy.inexact):
                    missing_value = dtype.type(numpy.nan)
                elif numpy.issubclass_(dtype.type, numpy.integer):
                    missing_value = dtype.type(-1)
                else:
                    missing_value = numpy.zeros((1, ), dtype=dtype)[0]
                if not isinstance(missing_value, numpy.bytes_):
                    vattrs['missing_value'] = missing_value
            if 'source_name' in v:
                vattrs['dship_source_name'] = v['source_name']
            else:
                vattrs['coordinates'] = k + '_source_name'
            v['dtype_str'] = dtype_str
            v['dtype'] = dtype
            v['attrs'] = vattrs
            v['missing_value'] = missing_value
            if 'grid_mapping' in vattrs:
                need_wgs84_grid_mapping = True
        # Now, because writing one element at a time is slow and causes
        # major fragmentation of the file and waste of space; we will
        # write the data in whole chunks. We need to allocate the
        # buffers that will hold each one while also keeping the missing
        # value around and whether the quantity is a number or not.
        buffers = {'time': (True, numpy.nan,
                            numpy.full((chunksize, ),
                                       numpy.nan, 'float64'))}
        for k, v in itertools.chain(qs_single.items(),
                                    qs_multi.items()):
            buffers[k] = (numpy.issubclass_(v['dtype'].type,
                                            numpy.number),
                          v['missing_value'],
                          numpy.full((chunksize, ),
                                     v['missing_value'], v['dtype']))
            if 'columns' in v:
                buffers[k + '_individual'] = (
                    numpy.issubclass_(v['dtype'].type, numpy.number),
                    v['missing_value'],
                    numpy.full((chunksize, len(v['columns'])),
                               v['missing_value'], v['dtype']))
                if v['has_precision']:
                    buffers[k + '_individual_precision_from_digits'] = (
                        numpy.issubclass_(v['dtype'].type,
                                          numpy.number),
                        numpy.float32(v['missing_value']),
                        numpy.full((chunksize, len(v['columns'])),
                                   numpy.float32(numpy.inf),
                                   'float32'))
            elif v['has_precision']:
                buffers[k + '_precision_from_digits'] = (
                    numpy.issubclass_(v['dtype'].type,
                                      numpy.number),
                    numpy.float32(v['missing_value']),
                    numpy.full((chunksize, ),
                               numpy.float32(v['missing_value']),
                               'float32'))
        # We also need a dict to hole the actual ranges for all
        # variables as we get ranges. Initially it is empty since we
        # have no data from any variable yet.
        actual_ranges = dict()
        # We will store the data from each iterator in a list that we
        # will then go through. We will read one line from each right
        # now so we can find the first time value.
        data = []
        for v in logits:
            try:
                data.append(next(v))
            except StopIteration:
                data.append(None)
        # Go through all the data and get the least first time. Then
        # choose a suitable epoch (zero point) that is rounded down to
        # the nearest day, and if possible month, and if possible year;
        # such that there are no leap seconds between the epoch and this
        # first time.
        dt_first = min([v[0] for v in data if v is not None])
        dt0, tai_utc_diff, leap_since_1970, next_leap = choose_epoch(
            dt_first)
        # Calculate the number of seconds between dt0 and dt_first.
        time_offset = int((dt_first - dt0).total_seconds())
        # Put the time information in the log.
        if logger is not None:
            logger.debug('Found time epoch:')
            logger.debug('  Epoch: '
                         + dt0.strftime('%Y-%m-%d %H:%M:%S Z'))
            logger.debug('  Data start after epoch: {0} s'.format(
                time_offset))
            logger.debug('  Offset from TAI to UTC: {0} s'.format(
                int(tai_utc_diff)))
            logger.debug('  Leap seconds since 1970: {0}'.format(
                int(leap_since_1970)))
            if numpy.isfinite(next_leap):
                logger.debug(
                    '  Next leap second after epoch: {0} s'.format(
                        next_leap))
            else:
                logger.debug('  Don''t know when next leap second is.')
        # Open the NetCDF4 file and set the File Attributes. If we have
        # latitute and longitude, it is a trajectory.
        if logger is not None:
            logger.info('Creating NetCDF4 output file and Variables.')
        with netCDF4.Dataset(output_path, 'w', clobber=True) as f:
            f.setncatts(attrs)
            if 'lat' in qs_info and 'lon' in qs_info:
                f.featureType = 'trajectory'
            # Make the grid mapping Variable if it is necessary.
            if need_wgs84_grid_mapping:
                crsvar = f.createVariable('crs', str)
                crsvar[()] = 'WGS 84'
                crsvar.grid_mapping_name = 'latitude_longitude'
                crsvar.reference_ellipsoid_name = 'WGS 84'
                crsvar.prime_meridian_name = 'Greenwich'
                crsvar.horizontal_datum_name = \
                    'World_Geodetic_System_1984'
                crsvar.geographic_crs_name = 'WGS 84'
                crsvar.longitude_of_prime_meridian = numpy.float64(0.0)
                crsvar.semi_major_axis = numpy.float64(6378137.0)
                crsvar.inverse_flattening = numpy.float64(298.257223563)
                crsvar.crs_wkt = \
                    'GEODCRS["WGS 84",' \
                    'DATUM["World Geodetic System 1984",' \
                    'ELLIPSOID["WGS 84",6378137,298.257223563,' \
                    'LENGTHUNIT["metre",1.0]]],' \
                    'CS[ellipsoidal,3],' \
                    'AXIS["(lat)",north,' \
                    'ANGLEUNIT["degree",0.0174532925199433]],' \
                    'AXIS["(lon)",east,' \
                    'ANGLEUNIT["degree",0.0174532925199433]],' \
                    'AXIS["ellipsoidal height (h)",up,' \
                    'LENGTHUNIT["metre",1.0]]]'
            # Make the time dimension and variable.
            f.createDimension('time', None)
            tvar = f.createVariable('time', 'float64',
                                    dimensions=('time', ),
                                    chunksizes=(chunksize, ),
                                    fill_value=numpy.nan,
                                    **vargs)
            tvar.standard_name = 'time'
            tvar.units = dt0.strftime('seconds since %Y-%m-%d %H:%M:%S')
            tvar.calendar = 'proleptic_gregorian'
            # tvar.axis = 'T'
            tvar.comment = 'The values are leap-second aware.'
            tvar.leap_seconds_since_posix_epoch = numpy.uint32(
                leap_since_1970)
            tvar.difference_between_UTC_and_TAI_at_zero = numpy.float64(
                tai_utc_diff)
            if numpy.isfinite(next_leap):
                tvar.first_leap_second_after_zero = numpy.float64(
                    next_leap)
            # Make all the variables for collected quantities.
            for k, v in qs_single.items():
                v['var'] = f.createVariable(
                    k, v['dtype_str'],
                    dimensions=('time', ),
                    chunksizes=(chunksize, ),
                    fill_value=v['missing_value'],
                    **vargs)
                v['var'].setncatts(v['attrs'])
                # Make the variable for the precision if required.
                if v['has_precision']:
                    v['var'].ancillary_variables = \
                        k + '_precision_from_digits'
                    v['var_prec'] = f.createVariable(
                        k + '_precision_from_digits', 'float32',
                        dimensions=('time', ),
                        chunksizes=(chunksize, ),
                        fill_value=numpy.float32(v['missing_value']),
                        **vargs)
                    v['var_prec'].missing_value = numpy.float32(
                        v['missing_value'])
                    if 'units' in v['attrs']:
                        v['var_prec'].units = v['attrs']['units']
                    v['var_prec'].comment = \
                        'Precision of the quantity value based on ' \
                        'the number of digits in the logs. This is ' \
                        'the maximum of the precision indicated by ' \
                        'the number of digits and the smallest ' \
                        'precision for the whole quantity when ' \
                        'trailing zeros are all removed. This is a ' \
                        'lower bound on the error.'
            for k, v in qs_multi.items():
                # Create the dimension for the individual sources (the
                # columns).
                f.createDimension(k + '_source', len(v['columns']))
                dvar = f.createVariable(
                    k + '_source', 'uint32', dimensions=k + '_source')
                dvar[:len(v['columns'])] = numpy.arange(
                    len(v['columns']), dtype='uint32')
                dvar.comment = 'Source number.'
                # Create the variable for the source names.
                nvar = f.createVariable(
                    k + '_source_name', str, dimensions=k + '_source')
                for i, v2 in enumerate(v['columns']):
                    nvar[i] = v2
                nvar.comment = 'DSHIP names of the sources.'
                # Create the variable, but without the axis Attribute.
                v['var'] = f.createVariable(
                    k + '_individual', v['dtype_str'],
                    dimensions=('time', k + '_source'),
                    chunksizes=(chunksize, len(v['columns'])),
                    fill_value=v['missing_value'],
                    **vargs)
                v['var'].setncatts({
                    k2: v2 for k2, v2 in v['attrs'].items()
                    if k2 != 'axis'})
                # Make the variable for the precision if required.
                if v['has_precision']:
                    v['var'].ancillary_variables = \
                        k + '_individual_precision_from_digits'
                    v['var_prec'] = f.createVariable(
                        k + '_individual_precision_from_digits',
                        'float32',
                        dimensions=('time', k + '_source'),
                        chunksizes=(chunksize, len(v['columns'])),
                        fill_value=numpy.float32(v['missing_value']),
                        **vargs)
                    v['var_prec'].missing_value = numpy.float32(
                        v['missing_value'])
                    if 'units' in v['attrs']:
                        v['var_prec'].units = v['attrs']['units']
                    v['var_prec'].comment = \
                        'Precision of the quantity value based on ' \
                        'the number of digits in the logs. This is ' \
                        'the maximum of the precision indicated by ' \
                        'the number of digits and the smallest ' \
                        'precision for the whole quantity when ' \
                        'trailing zeros are all removed. This is a ' \
                        'lower bound on the error.'
                # Create the 1D version of the variable, but without the
                # coordinates Attribute and also setting the
                # ancillary_variables Attribute.
                v['var_1d'] = f.createVariable(
                    k, v['dtype_str'],
                    dimensions=('time', ),
                    chunksizes=(chunksize, ),
                    fill_value=v['missing_value'],
                    **vargs)
                v['var_1d'].setncatts({
                    k2: v2 for k2, v2 in v['attrs'].items()
                    if k2 != 'coordinates'})
                v['var_1d'].ancillary_variables = k + '_individual'
                if 'single_val' in v:
                    v['var_1d'].comment = 'Always the value from the ' \
                        + v['single_val'] + ' column of ' + k \
                        + '_individual.'
                else:
                    v['var_1d'].comment = 'Average of the valid ' \
                        'columns from ' + k + '_individual.'
            # We need to build a lookup for all variables we are writing
            # to.
            all_vars = {'time': tvar}
            for k, v in itertools.chain(qs_single.items(),
                                        qs_multi.items()):
                if 'var_1d' in v:
                    all_vars[k + '_individual'] = v['var']
                    all_vars[k] = v['var_1d']
                    if 'var_prec' in v:
                        all_vars[k + '_individual_precision_'
                                 'from_digits'] = v['var_prec']
                else:
                    all_vars[k] = v['var']
                    if 'var_prec' in v:
                        all_vars[k + '_precision_from_digits'] = \
                            v['var_prec']
            # Now, we iterate forward in time one second at a time until
            # we run out of data. On each iteration of the loop, we do
            # the following:
            #
            # 1. Log a progress update if it has been a minute since
            #    the last one.
            # 2. Save the buffers to disk if they are full or we are out
            #    of data.
            # 3. Stop if we are out of data.
            # 4. Check to make sure we haven't reached a leap second.
            # 5. Read a new line from any logs that are at the present.
            # 6. Increment the time by one.
            t = 0
            dt = dt_first
            timedelta_one = datetime.timedelta(seconds=1)
            cnt = 0
            if logger is not None:
                t_last_print = time.perf_counter()
                logger.info('Processing data on second pass:')
            while True:
                if logger is not None:
                    tnow = time.perf_counter()
                    if tnow - t_last_print >= 60.0:
                        if t <= 60:
                            logger.info('  Processed: {0} s'.format(t))
                        elif t < 3600:
                            logger.info(
                                '  Processed: {0:1.2f} min'.format(
                                    t / 60.0))
                        elif t < 24 * 3600:
                            logger.info(
                                '  Processed: {0:1.2f} hr'.format(
                                    t / 3600.0))
                        else:
                            logger.info(
                                '  Processed: {0:1.2f} days'.format(
                                    t / (24 * 3600.0)))
                        t_last_print = tnow
                # Check if we are out of data.
                out_of_data = all([v is None for v in data])
                # Write data from the buffers if we are out or if we
                # have filled them.
                if out_of_data or cnt == chunksize:
                    # Build the slices to read from the buffers and for
                    # where to write in the variables.
                    from_slice = slice(0, cnt)
                    to_slice = slice(t - cnt, t)
                    # Go through each variable. They will not be written
                    # if we still have data and they are entirely the
                    # missing value. If the data is numerical, we need
                    # to determine the actual range using the previous
                    # information and the data in the buffer. Then, we
                    # must clear the buffer back to the missing value.
                    for k, (isnum, mv, buf) in buffers.items():
                        if out_of_data:
                            write = True
                        elif isnum and numpy.isnan(mv) and numpy.isnan(
                                buf[from_slice]).all():
                            write = False
                        elif (buf[from_slice] == mv).all():
                            write = False
                        else:
                            write = True
                        if write:
                            data_to_write = buf[from_slice]
                            all_vars[k][to_slice] = data_to_write
                            if isnum:
                                data_to_write = data_to_write.ravel()
                                if numpy.isnan(mv):
                                    valdata = data_to_write[
                                        ~numpy.isnan(data_to_write)]
                                else:
                                    valdata = data_to_write[
                                        ~numpy.logical_or(
                                            numpy.isnan(data_to_write),
                                            data_to_write == mv)]
                                if valdata.size != 0:
                                    arb = numpy.empty((2, ),
                                                      dtype=buf.dtype)
                                    arb[0] = valdata.min()
                                    arb[1] = valdata.max()
                                    if k in actual_ranges:
                                        ar = actual_ranges[k]
                                        if arb[0] < ar[0]:
                                            ar[0] = arb[0]
                                        if arb[1] > ar[1]:
                                            ar[1] = arb[1]
                                    else:
                                        actual_ranges[k] = arb
                        buf.fill(mv)
                    # Reset the count.
                    cnt = 0
                # Stop if we are out of data.
                if out_of_data:
                    break
                # Check that a leap second hasn't occurred.
                if t + time_offset >= next_leap:
                    raise NotImplementedError(
                        'Cannot process logs that have a leap second '
                        'in them.')
                # Store the time value in the buffer.
                buffers['time'][2][cnt] = t + time_offset
                # Go through all single source quantities and get all
                # available values from the logs. Then remove all
                # invalid values and if there are any left, take the
                # mean, and store it in the buffer.
                for k, v in qs_single.items():
                    if v['has_precision']:
                        vals = []
                        precs = []
                        for i, j, _, _, _ in v['source']:
                            if data[i] is not None and data[i][0] == dt:
                                val = data[i][1][j]
                                if isinstance(val, numpy.ndarray):
                                    vals.append(val[0])
                                    precs.append(val[1])
                                else:
                                    vals.append(val)
                                    precs.append(1.0)
                        precs = numpy.float32(precs)
                    else:
                        vals = [data[i][1][j]
                                for i, j, _, _, _ in v['source']
                                if data[i] is not None
                                and data[i][0] == dt]
                        precs = None
                    if buffers[k][0] and numpy.isnan(buffers[k][1]):
                        if precs is None:
                            valids = [v2 for v2 in vals
                                      if not numpy.isnan(v2)]
                        else:
                            valids = []
                            valid_precs = []
                            for i, v2 in enumerate(vals):
                                if not numpy.isnan(v2):
                                    valids.append(v2)
                                    valid_precs.append(precs[i])
                    else:
                        if precs is None:
                            valids = [v2 for v2 in vals
                                  if v2 != buffers[k][1]]
                        else:
                            valids = []
                            valid_precs = []
                            for i, v2 in enumerate(vals):
                                if v2 != buffers[k][1]:
                                    valids.append(v2)
                                    valid_precs.append(precs[i])
                    # If we have precisions, take only the valid values
                    # for the smallest precision.
                    if len(valids) != 0 and precs is not None:
                        min_prec = numpy.min(valid_precs)
                        new_valids = []
                        if numpy.isinf(min_prec):
                            for i, v2 in enumerate(valid_precs):
                                if numpy.isinf(v2):
                                    new_valids.append(valids[i])
                        else:
                            for i, v2 in enumerate(valid_precs):
                                if v2 == min_prec:
                                    new_valids.append(valids[i])
                        valids = new_valids
                    if len(valids) == 1 or len(set(valids)) == 1:
                        buffers[k][2][cnt] = valids[0]
                        if precs is not None:
                            buffers[k + '_precision_from_digits'][2][
                                cnt] = min_prec
                    elif len(valids) > 1:
                        if buffers[k][0]:
                            buffers[k][2][cnt] = numpy.mean(valids)
                            if precs is not None:
                                buffers[k + '_precision_from_digits'][
                                    2][cnt] = min_prec
                        else:
                            raise RuntimeError(
                                'Logs are inconsistent for ' + k
                                + 'at time ' + str(dt) + ': '
                                + str(valids))
                # We need to do the similar thing for multi source
                # quantities, but must build the list of values for each
                # source separately. Missing values will be filtered out
                # when building the list instead of afterwards.
                for k, v in qs_multi.items():
                    vals = dict()
                    isnum = buffers[k][0]
                    mv = buffers[k][1]
                    mvisnan = (isnum and numpy.isnan(mv))
                    if v['has_precision']:
                        precs = dict()
                        for i, j, k2, _, hp in v['sources']:
                            if data[i] is not None and data[i][0] == dt:
                                val = data[i][1][j]
                                if hp:
                                    if (mvisnan and
                                        not numpy.isnan(val[0])) or (
                                            not mvisnan
                                            and val[0] != mv):
                                        if k2 in vals:
                                            vals[k2].append(val[0])
                                            precs[k2].append(val[1])
                                        else:
                                            vals[k2] = [val[0]]
                                            precs[k2] = [val[1]]
                                else:
                                    if (mvisnan and
                                        not numpy.isnan(val)) or (
                                            not mvisnan and val != mv):
                                        if k2 in vals:
                                            vals[k2].append(val)
                                            precs[k2].append(1.0)
                                        else:
                                            vals[k2] = [val]
                                            precs[k2] = [1.0]
                        # Take only the valid values for the smallest
                        # precision.
                        for k2 in precs:
                            values = vals[k2]
                            value_precs = numpy.float32(precs[k2])
                            min_prec = numpy.min(value_precs)
                            new_values = []
                            if numpy.isinf(min_prec):
                                for i, v2 in enumerate(value_precs):
                                    if numpy.isinf(v2):
                                        new_values.append(values[i])
                            else:
                                for i, v2 in enumerate(value_precs):
                                    if v2 == min_prec:
                                        new_values.append(values[i])
                            vals[k2] = new_values
                            precs[k2] = min_prec
                    else:
                        precs = None
                        for i, j, k2, _, _ in v['sources']:
                            if data[i] is not None and data[i][0] == dt:
                                val = data[i][1][j]
                                if (mvisnan and
                                    not numpy.isnan(val)) or (
                                        not mvisnan and val != mv):
                                    if k2 in vals:
                                        vals[k2].append(val)
                                    else:
                                        vals[k2] = [val]
                    # Now we have the valid values for each source and
                    # can set the columns one by one. We need to collect
                    # the means for each column to assign.
                    buf = buffers[k + '_individual'][2]
                    means = dict()
                    if precs is not None:
                        buf_precs = buffers[
                            k + '_individual_precision_from_digits'][2]
                        mean_precs = dict()
                    for c, colname in enumerate(v['columns']):
                        if colname in vals:
                            colvals = vals[colname]
                            if len(colvals) != 0:
                                if len(colvals) == 1 or len(
                                        set(colvals)) == 1:
                                    val_to_assign = colvals[0]
                                elif len(colvals) > 1:
                                    if isnum:
                                        val_to_assign = numpy.mean(
                                            colvals)
                                    else:
                                        raise RuntimeError(
                                            'Logs are inconsistent for '
                                            + k + 'at time ' + str(dt)
                                            + ': ' + str(colvals))
                                buf[cnt, c] = val_to_assign
                                means[colname] = val_to_assign
                                if precs is not None:
                                    prec_to_assign = precs[colname]
                                    buf_precs[cnt, c] = prec_to_assign
                                    mean_precs[colname] = prec_to_assign
                    # And the mean of all columns is the single value
                    # unless we are selecting a single column.
                    if 'single_val' in v:
                        if v['single_val'] in means:
                            buffers[k][2][cnt] = means[v['single_val']]
                    elif len(means) == 1 or len(set(means)) == 1:
                        buffers[k][2][cnt] = list(means.values())[0]
                    elif len(means) > 1:
                        if isnum:
                            # Select the values with the smallest
                            # precision.
                            if precs is None:
                                buffers[k][2][cnt] = numpy.mean(
                                    list(means.values()))
                            else:
                                mps = numpy.float32([
                                    mean_precs[colname]
                                    for colname in v['columns']
                                    if colname in means])
                                vs = [means[colname]
                                      for colname in v['columns']
                                      if colname in means]
                                min_prec = numpy.min(mps)
                                new_vs = []
                                if numpy.isinf(min_prec):
                                    for i, v2 in enumerate(mps):
                                        if numpy.isinf(v2):
                                            new_vs.append(vs[i])
                                else:
                                    for i, v2 in enumerate(mps):
                                        if v2 == min_prec:
                                            new_vs.append(vs[i])
                                buffers[k][2][cnt] = numpy.mean(new_vs)
                        else:
                            raise RuntimeError(
                                'Logs are inconsistent for ' + k
                                + 'at time ' + str(dt) + ': '
                                + str(means))
                # Increment the time, the logs if they are current, and
                # go to the next iteration.
                for i, v in enumerate(logits):
                    if data[i] is not None and data[i][0] == dt:
                        try:
                            data[i] = next(v)
                        except StopIteration:
                            data[i] = None
                dt += timedelta_one
                t += 1
                cnt += 1
            # Log that processing is complete.
            if logger is not None:
                if t <= 60:
                    logger.info('  Complete: {0} s'.format(t))
                elif t < 3600:
                    logger.info('  Complete: {0:1.2f} min'.format(
                        t / 60.0))
                elif t < 24 * 3600:
                    logger.info('  Complete: {0:1.2f} hr'.format(
                        t / 3600.0))
                else:
                    logger.info('  Complete: {0:1.2f} days'.format(
                        t / (24 * 3600.0)))
            # Go through each variable with an actual_range and set the
            # Attribute.
            if logger is not None:
                logger.info('Setting actual_range Attributes.')
            for k, v in actual_ranges.items():
                all_vars[k].actual_range = v
            if logger is not None:
                logger.info('Done.')
    except:
        # Put the traceback in the logger, close the logger, and reraise
        # the error.
        if logger is not None:
            logger.critical('There was an error.')
            for line in (''.join(traceback.format_exception(
                    *sys.exc_info()))).split('\n'):
                logger.critical(line)
            for v in logger.handlers:
                v.flush()
                v.close()
            logger = None
        raise
    finally:
        for v in logits:
            try:
                v.close()
            except:
                pass
        # Close the logger.
        if logger is not None:
            for v in logger.handlers:
                v.flush()
                v.close()
            logger = None


class DshipLogIterator(collections.abc.Iterator):
    """ An Iterator over a DSHIP log.

    An Iterator over a DSHIP log that gives the data line by line. On
    each iteration, two values are returned: the time
    (``datetime.datetime``) and a ``list`` of the column values. Column
    values are either ``str``, ``int``, ``datetime.datetime``, or
    2-element float64 ``numpy.ndarray`` composed of the value and the
    precision.

    The log can be processed in one or two pass mode. In one pass mode,
    the precisions are based purely on the number of significant figures
    in the logs. In two pass, a first pass is done when creating the
    object to find the smallest precision when trailing zeros are
    stripped off the numbers and those smallest precisions being used as
    lower bounds for the precisions on the second pass (clipping the
    values on the lower end).

    This class supports context management using the ``with`` statement.

    Warning
    -------
    When `two_pass` is ``True``, the first pass when calling the
    constructor for this class in can take a lot of time because the
    whole file must be processed for iteration can proceed. It takes
    approximately as long as iteration over the whole file with this
    instance does (partly because that is how it is implemented).

    Parameters
    ----------
    path : str
        The path to the DSHIP log to process. ZIP and TAR (compressed or
        not) files are supported, in which case the first file with the
        ``'.dat'`` extension inside them is treated as the
        log. Transparent decompression of gzip-ed and bzip2-ed logs is
        done.
    two_pass : bool, optional
        Whether to do two passes to get the lower precision limits for
        each quantity or not (default). When two passes are done,
        ``best_precisions`` from the first pass is used as the lower
        precision limits.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If the file does not have the right format.
    IOError
        If the file cannot be found or there is an IO error.
    KeyError
        If a suitable file cannot be found in `path` if it is an
        archive.

    Attributes
    ----------
    kinds : list
        The kind for each column (``str``). ``'spot'`` means a single
        measurement, ``'mean'`` means a mean over some range, etc.
    missing : list
        The missing values for each column.
    quantity_names : list
        Suitable names, if any, for each column (``str``) that follow CF
        Conventions. For those with a standard name, this will be their
        standard name. For others, something reasonable is
        suggested. For some, it will just be their source name.
    source_names : list
        The DSHIP names for each column (``str``).
    types : list
        The type of each column that needs to be passed to ``netCDF4``
        when making a variable for it. Each is a ``str`` that is
        suitable to pass to ``numpy.dtype`` or is literally ``str``.
    units : list
        The units for each column (``str`` or ``None``) after
        conversions. Note that unit names have been converted to CF
        Conventions.
    best_precisions : list
        The best precision for each quantity so far after trailing zeros
        have been removed. The values are ``float`` for quantities that
        have a precision and ``None`` for those that do not.
    non_missing : list
        The number of non-missing values for each quantity so far. The
        values are ``int``.
    any_non_missing : list
        ``bool`` mask of the quantities that have any non-missing
        values. If doing `two_pass`, this is whether the first pass
        found any non-missing values. If doing only a single pass, this
        is whether there have been any non-missing values so far.

    """
    def __init__(self, path, two_pass=False):
        # We must first initialize the two "file" handles (only one is
        # used for actual files and two are used for archives) and then
        # a text wrapper so that no matter what happens, close can run
        # without crashing. We will similarly a file handle for the
        # order XML file if it is available.
        self._archive_fd = None
        self._fd = None
        self._fd_xml = None
        self._wrapper = None
        # Check two_pass:
        if not isinstance(two_pass, bool):
            raise TypeError('two_pass must be bool.')
        # If doing two passes, do the first pass making a new version of
        # this class with the same file, iterate over the whole file,
        # copy best_precisions to the precision limits variable
        # (will be None otherwise), and copy any_non_missing.
        if two_pass:
            with DshipLogIterator(path, two_pass=False) as firstpass:
                for _ in firstpass:
                    pass
                self._lower_precision_limits = firstpass.best_precisions
                self._any_non_missing = firstpass.any_non_missing
        else:
            self._lower_precision_limits = None
            self._any_non_missing = None
        # Check path.
        if not isinstance(path, str):
            raise TypeError('path must be str.')
        if not os.path.isfile(path):
            raise IOError('path doesn''t point to a file.')
        # Try reading it as a ZIP file first. If that fails, try as a
        # tarball. If that fails, as a normal possibly compressed file.
        try:
            self._archive_fd = zipfile.ZipFile(path, mode='r')
        except:
            try:
                self._archive_fd = tarfile.open(name=path, mode='r')
            except:
                self._fd = fileinput.hook_compressed(path, 'rb')
        # If we got here, it opened in some way. If it is a zip or tar
        # file, we must search it for the first .dat file and first .xml
        # file if any.
        if isinstance(self._archive_fd, zipfile.ZipFile):
            for info in self._archive_fd.infolist():
                if self._fd is not None and self._fd_xml is not None:
                    break
                elif self._fd is None and info.filename.endswith('.dat'):
                    self._fd = self._archive_fd.open(info, 'r')
                elif self._fd_xml is None and info.filename.endswith(
                        '.xml'):
                    self._fd_xml = self._archive_fd.open(info, 'r')
        elif isinstance(self._archive_fd, tarfile.TarFile):
            for info in self._archive_fd:
                if self._fd is not None and self._fd_xml is not None:
                    break
                elif info.isfile():
                    if self._fd is None and info.name.endswith('.dat'):
                        self._fd = self._archive_fd.extractfile(info)
                    elif self._fd_xml is None \
                            and info.name.endswith('.xml'):
                        self._fd_xml = self._archive_fd.extractfile(
                            info)
        # Check that we even found a file.
        if self._fd is None:
            raise KeyError('Could not find a suitable log file in the '
                           'archive.')
        # If the XML order file is available, go through it and look for
        # the channellist line and extract the error values used in the
        # fields.
        self._errorvaluenumeric = None
        self._errorvaluealphanumeric = None
        if self._fd_xml is not None:
            with io.TextIOWrapper(self._fd_xml, encoding='ISO-8859-1',
                                  newline='\n') as wrp:
                for line in wrp:
                    line = line.strip()
                    if line.startswith('<channellist ') \
                            and line.endswith('>'):
                        # The different fields are space separated, with
                        # the pairs being separated by equal signs.
                        fields = line[:-1].split(' ')[1:]
                        for field in fields:
                            if self._errorvaluenumeric is not None \
                                    and self._errorvaluealphanumeric \
                                    is not None:
                                break
                            parts = field.split('=')
                            if len(parts) != 2 \
                                    or not parts[1].startswith('"') \
                                    or not parts[1].endswith('"'):
                                continue
                            value = parts[1][1:-1]
                            if parts[0] == 'errorvaluenumeric':
                                self._errorvaluenumeric = value
                            elif parts[0] == 'errorvaluealphanumeric':
                                self._errorvaluealphanumeric = value
                        break
            try:
                self._fd_xml.close()
            except:
                pass
            self._fd_xml = None
        # Make a text wrapper on the file using the ISO-8859-1 encoding
        # and newline terminators.
        self._wrapper = io.TextIOWrapper(self._fd,
                                         encoding='ISO-8859-1',
                                         newline='\n')
        # The first three lines (header) of the file contain the
        # metadata. The first line contains the column headers. The
        # second line indicates whether each column is an instantaneous
        # value (spot), mean, etc. The third line is the units for each
        # column.
        #
        # Additionally, these lines tell us the separator that the file
        # uses; which is important to get. The separator is either
        # semicolons, commas, or colons.
        name_line = next(self._wrapper)[:-1]
        kind_line = next(self._wrapper)[:-1]
        unit_line = next(self._wrapper)[:-1]
        # See if semicolons, commas, colons, or tabs are in the
        # lines. We must have one and only one in each line. Then, we
        # know the separator.
        possible_seps = (',', ';', ':', '\t')
        founds = len(possible_seps) * [False]
        for line in (name_line, kind_line, unit_line):
            found_this_line = [v in line for v in possible_seps]
            if not any(found_this_line):
                raise ValueError('A header line had no separators.')
            for i, v in enumerate(found_this_line):
                founds[i] = (founds[i] or v)
        if sum(founds) > 1:
            raise ValueError('Delimiter is ambiguous.')
        for i, v in enumerate(founds):
            if v:
                self._sep = possible_seps[i]
        # Split the header lines and convert to str.
        names = [v for v in name_line.split(self._sep)]
        kinds = [v for v in kind_line.split(self._sep)]
        units = [v for v in unit_line.split(self._sep)]
        # Each must have the same number of elements.
        if len(names) != len(kinds) or len(names) != len(units):
            raise ValueError('Header lines didn''t have the same '
                             'number of elements.')
        # The first column must be one of the recognized time
        # columns. Depending on the name, we must read the time
        # different ways to get a datetime.datetime.
        if len(names) == 0:
            raise ValueError('File must have at least one column.')
        elif names[0] == 'seconds since 1970':
            self._time_conv = lambda s: \
                datetime.datetime.utcfromtimestamp(float(s)).replace(
                    tzinfo=datetime.timezone.utc)
        elif names[0] == 'date time':
            self._time_conv = self._read_datetime
        elif names[0] == 'date':
            self._time_conv = lambda s: datetime.datetime.strptime(
                s, '%Y%m%dT%H%M%S').replace(
                    tzinfo=datetime.timezone.utc)
        else:
            raise NotImplementedError(
                "Cannot handle time column with the format '"
                + names[0] + "'.")
        # We need to define the information for each column including
        # their names, units, extraction/conversion functions, quantity
        # name, and type names (suitable to pass to netCDF4).
        self._convs = []
        self._tps = []
        self._units = []
        self._source_names = []
        self._quantity_names = []
        self._kinds = kinds[1:]
        self._missing = []
        self._best_precisions = []
        for i in range(1, len(names)):
            # We need to consider both the name and the name with the
            # characters before the first period stripped off.
            name = names[i]
            name_lookup = _find_name_in_src_lookup(name)
            best_prec = None
            # Lookup the quantity name, types, and units
            if name_lookup is not None:
                k = _src_lookup[name_lookup]
                if isinstance(k, str):
                    qname = k
                else:
                    qname = name
                if qname in _quantity_lookup:
                    qtp, attrs = _quantity_lookup[qname]
                    if 'units' in attrs:
                        qunit = attrs['units']
                    else:
                        qunit = None
                elif name_lookup in _quantity_lookup:
                    qtp, attrs = _quantity_lookup[name_lookup]
                    if 'units' in attrs:
                        qunit = attrs['units']
                    else:
                        qunit = None
                else:
                    qtp = None
                    qunit = None
            else:
                qname = name
                qtp = None
                qunit = None
            # Do simple conversions for deg and special characters in
            # unit while getting it.
            unit = units[i].replace('deg', 'degree').replace(
                '°C', 'C').replace('C°', 'C').replace(
                    '°', 'degree').replace('²', '^2')
            # Work out the type for each units and any needed
            # conversions.
            if unit == '':
                unit = qunit
                if qtp is None or qtp == str:
                    tp = str
                    fun = self._read_str
                    missing = ''
                elif numpy.issubclass_(
                        numpy.dtype(qtp).type, numpy.inexact):
                    tp = qtp
                    fun = self._read_float
                    missing = self._nan_arr
                    best_prec = float('inf')
                else:
                    tp = qtp
                    fun = self._read_int
                    missing = -1
                    best_prec = None
            else:
                # We will assume it is floating point for now. If the
                # unit isn't something that requires a conversion, we
                # will check if it is supposed to be floating point or
                # integer from the quantity lookup.
                tp = 'float64'
                missing = self._nan_arr
                best_prec = float('inf')
                if unit == 'C':
                    unit = 'K'
                    fun = lambda s: self._read_float(s) + (273.15, 0, 0)
                elif unit == 'kn':
                    unit = 'm/s'
                    fun = lambda s: \
                        scipy.constants.knot * self._read_float(s)
                elif unit == '%':
                    unit = '1'
                    fun = lambda s: 1e-2 * self._read_float(s)
                elif unit in ('hPa', 'mbar'):
                    unit = 'Pa'
                    fun = lambda s: 100 * self._read_float(s)
                elif unit in ('mW/m2', 'mW/m^2'):
                    unit = 'W/m^2'
                    fun = lambda s: 1e-3 * self._read_float(s)
                elif unit == 'km/h':
                    unit = 'm/s'
                    fun = lambda s: \
                        scipy.constants.kmh * self._read_float(s)
                elif unit == 'kHz':
                    unit = 'Hz'
                    fun = lambda s: 1000 * self._read_float(s)
                elif unit == 'degree/min':
                    unit = 'degree/s'
                    fun = lambda s: self._read_float(s) / 60.0
                elif unit == 'ft':
                    unit = 'm'
                    scale = numpy.dtype(qtp).type(scipy.constants.foot)
                    fun = lambda s, a=scale: a * self._read_float(s)
                elif unit == 'F':
                    unit = 'm'
                    scale = numpy.dtype(qtp).type(
                        6 * scipy.constants.foot)
                    fun = lambda s, a=scale: a * self._read_float(s)
                elif unit == 'mS/cm':
                    unit = 'S/m'
                    fun = lambda s: 0.1 * self._read_float(s)
                elif qunit in ('degree_north', 'degree_east'):
                    fun = lambda s: self._read_latlon(s)
                elif qtp is None or numpy.issubclass_(
                        numpy.dtype(qtp).type, numpy.inexact):
                    fun = self._read_float
                else:
                    fun = self._read_int
                    missing = -1
                    best_prec = None
                # The unit of '-' means nothing.
                if unit == '-':
                    unit = None
            # If the quantity unit or type were gotten, they override
            # what we got above.
            if qunit is not None:
                unit = qunit
            if qtp is not None:
                tp = qtp
            # Pack in the information
            self._convs.append(fun)
            self._tps.append(tp)
            self._units.append(unit)
            self._source_names.append(name)
            self._quantity_names.append(qname)
            self._missing.append(missing)
            self._best_precisions.append(best_prec)
        # Initialize non_missing and make functions that can assess
        # whether a value is missing or not.
        self._non_missing = len(self._kinds) * [0]
        self._is_missing_funcs = []
        for v in self._missing:
            if isinstance(v, (str, int)) or (isinstance(v, float)
                                             and not math.isnan(v)):
                self._is_missing_funcs.append(lambda x, m=v: x == m)
            elif isinstance(v, float):
                self._is_missing_funcs.append(math.isnan)
            elif isinstance(v, numpy.ndarray) and numpy.isnan(
                    v).all():
                self._is_missing_funcs.append(
                    lambda x: numpy.isnan(x).all())
            else:
                raise NotImplementedError('Do not know how to handle '
                                          'this missing value.')

    @property
    def kinds(self):
        return self._kinds.copy()

    @property
    def missing(self):
        return self._missing.copy()

    @property
    def quantity_names(self):
        return self._quantity_names.copy()

    @property
    def source_names(self):
        return self._source_names.copy()

    @property
    def types(self):
        return self._tps.copy()

    @property
    def units(self):
        return self._units.copy()

    @property
    def best_precisions(self):
        return self._best_precisions.copy()

    @property
    def non_missing(self):
        return self._non_missing.copy()

    @property
    def any_non_missing(self):
        if self._any_non_missing is None:
            return [v != 0 for v in self._non_missing]
        return self._any_non_missing.copy()

    def __enter__(self):
        return self

    def __exit__(self, tp, value, traceback):
        self.close()

    def __del__(self):
        try:
            self.close()
        except:
            pass

    def close(self):
        """ Closes the file. """
        for v in (self._wrapper, self._fd, self._fd_xml,
                  self._archive_fd):
            if v is not None:
                try:
                    v.close()
                except:
                    pass
        self._wrapper = None
        self._fd = None
        self._fd_xml = None
        self._archive_fd = None

    # We store the array [NaN, Nan, Nan] so we don't have to keep
    # re-evaluating it.
    _nan_arr = numpy.float64((numpy.nan, numpy.nan, numpy.nan))

    def _read_str(self, s):
        # Reads the the given string while looking for invalid values,
        # which means a blank string will be returned.
        if s in {'', 'format error', '-999.',
                 '-999-999-999-999-999-999-999.-999-999',
                 '-999-999-999-999.-999-999',
                 '99.99', '9999999.99'}:
            return ''
        elif self._errorvaluealphanumeric is not None \
                and s == self._errorvaluealphanumeric:
            return ''
        else:
            return s

    def _read_float(self, s):
        # Reads the number in the given string while looking for invalid
        # values, which means nan, inf will be returned.
        #
        # For valid values, they need to be parsed to get both the value
        # and the precision based on significant figures, unless the
        # result is NaN or infinite in which case the value is returned
        # three times. The precision will also be found with trailing
        # zeros removed.
        if s in {'', 'format error', '-999.',
                 '-999-999-999-999-999-999-999.-999-999',
                 '-999-999-999-999.-999-999',
                 '99.99', '9999999.99'}:
            return self._nan_arr
        elif self._errorvaluenumeric is not None \
                and s == self._errorvaluenumeric:
            return self._nan_arr
        else:
            value = float(s)
            if not math.isfinite(value):
                return numpy.float64((value, value, value))
            sci_parts = s.split('e')
            if len(sci_parts) == 2:
                exponent = int(sci_parts[1])
            else:
                exponent = 0
            parts = sci_parts[0].split('.')
            if len(parts) == 2:
                exponent -= len(parts[1])
            # Determine the number of trailing zeros by stripping zeros
            # off the right of the number part with decimal points and
            # sign removed. If the number is all zeros, then we leave
            # the leading zero.
            just_digits = ''.join(parts).rstrip('-+')
            numzeros = len(just_digits) - len(just_digits.rstrip('0'))
            if numzeros == len(just_digits):
                numzeros -= 1
            return numpy.float64((value, 10.0**exponent,
                                  10**(exponent + numzeros)))

    def _read_int(self, s):
        # Reads the number in the given string while looking for invalid
        # values, which means -1 will be returned.
        if s in {'', 'format error', '-999.',
                 '-999-999-999-999-999-999-999.-999-999',
                 '-999-999-999-999.-999-999',
                 '99.99', '9999999.99'}:
            return -1
        elif self._errorvaluenumeric is not None \
                and s == self._errorvaluenumeric:
            return -1
        else:
            return int(s)


    def _read_latlon(self, s):
        # Reads a latitude or longitude, which could be just a plain
        # float or it could be in the form
        #
        # "059° 37.748434' W"
        #
        # If it is a float, _read_float will succeed. Otherwise, an
        # exception will be raised and we have to check if it is in this
        # format.
        try:
            return self._read_float(s)
        except:
            pass
        main_parts = s.split('° ')
        if len(main_parts) != 2:
            return self._nan_arr
        second_parts = main_parts[1].split("' ")
        if len(second_parts) != 2 or second_parts[1] not in 'NSEW':
            return self._nan_arr
        result = (self._read_float(second_parts[0]) / 60.0) + [
            float(main_parts[0]), 0.0, 0.0]
        if second_parts[1] in 'SW':
            result[0] = -result[0]
        return result

    def _read_datetime(self, s):
        # Reads a datetime in either '%Y/%m/%d %H:%M:%S' or
        # '%d/%m/%Y %H:%M:%S' formats.
        for fmt in ('%Y/%m/%d %H:%M:%S', '%d/%m/%Y %H:%M:%S'):
            try:
                return datetime.datetime.strptime(
                    s, fmt).replace(
                        tzinfo=datetime.timezone.utc)
            except:
                pass
        # No format was valid.
        raise ValueError('Time value was invalid.')

    def __iter__(self):
        # Nothing special needs to be done other than return self since
        # everything was already setup in __init__.
        return self

    def __next__(self):
        # Can't do anything if the file is not open.
        if self._wrapper is None:
            raise StopIteration()
        # Read the next line of the file. If we get a StopIteration,
        # EOFError (compressed file ended before end marker), TarError
        # (something wrong with the tarball), or BadZipFile (something
        # wrong with the zip file); we are done and thus should close
        # and raise a StopIteration. Any other error should not be
        # converted, but the file should still be closed. We will repeat
        # on empty lines.
        line = ''
        try:
            while len(line) == 0 or line.isspace():
                line = next(self._wrapper)[:-1]
        except EOFError:
            self.close()
            raise StopIteration()
        except tarfile.TarError:
            self.close()
            raise StopIteration()
        except zipfile.BadZipFile:
            self.close()
            raise StopIteration()
        except:
            self.close()
            raise

        # Split into columns, read the time, and then read each
        # column. Columns need to be passed through their conversion
        # functions and missing values or errors converted to '' or
        # NaN. If the separator is not a comma, we will replace all
        # commas with decimal points to handle the case where numbers
        # are written as 9,3 instead of 9.3.
        if self._sep == ',':
            cols = line.split(self._sep)
        else:
            cols = line.replace(',', '.').split(self._sep)
        t = self._time_conv(cols[0])
        out = []
        for i, v in enumerate(self._convs):
            # If there are enough columns, it can be read. If not, we
            # put in a missing value.
            if i + 1 < len(cols):
                # If we get an error reading it, then put in a missing
                # value.
                try:
                    vals = v(cols[i + 1])
                    if not isinstance(vals, numpy.ndarray) \
                            or len(vals) == 1:
                        out.append(vals)
                    else:
                        # If we have a lower precision limit, raise the
                        # precision to that value if it is lower.
                        if self._lower_precision_limits is not None \
                                and numpy.isfinite(vals[1]):
                            vals[1] = max(
                                vals[1],
                                self._lower_precision_limits[i])
                        out.append(vals[:2])
                        if numpy.isfinite(vals[2]):
                            self._best_precisions[i] = min(
                                self._best_precisions[i], vals[2])
                    if not self._is_missing_funcs[i](vals):
                        self._non_missing[i] += 1
                except:
                    out.append(self._missing[i])
            else:
                out.append(self._missing[i])
        return t, out


def choose_epoch(date):
    """ Chooses a suitable epoch for the given date.

    Chooses an epoch for the given date that is before it but has the
    same number of leap seconds and is rounded to the nearest year, or
    if that is not possible, month and so on.

    Parameters
    ----------
    date : datetime.date
        The date to pick an epoch for. Values before 1960 cannot be
        handled.

    Returns
    -------
    dt : datetime.date
        The epoch date and time.
    tai_utc_diff : int or float
        The difference between TAI and UTC in seconds at the given
        `date`. A positive number means TAI is ahead.
    leap_seconds_since_posix_epoch : int
        The number of leap seconds on the given `date` since the POSIX
        epoch.
    next_leap : float
        The time between `dt` and the next leap second in seconds. It is
        set to infinity if no leap second after it has yet been
        announced.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.

    See Also
    --------
    astropy.utils.iers.LeapSeconds

    """
    if not isinstance(date, datetime.date):
        raise TypeError('date must be a datetime.date.')
    if date.year < 1960:
        raise ValueError('dates before 1960 are not supported.')
    # If date is a datetime, we need to extract the date part of it.
    if isinstance(date, datetime.datetime):
        date = date.date()
    # Get the leapsecond table.
    leaps = numpy.asarray(astropy.utils.iers.LeapSeconds.auto_open())
    # Extract the dates since we need to find the index of the last leap
    # second before it.
    dates = [datetime.date(year=v['year'], month=v['month'], day=1)
             for v in leaps]
    index = bisect.bisect_left(dates, date) - 1
    # Try to round the date down to the nearest year (and if not, month)
    # but still not being before the last leap second date.
    last_leap_date = dates[index]
    if date.replace(month=1, day=1) >= last_leap_date:
        month = 1
        day = 1
    elif date.replace(day=1) >= last_leap_date:
        month = date.month
        day = 1
    else:
        month = date.month
        day = date.day
    dt = datetime.datetime(year=date.year, month=month, day=day,
                           tzinfo=datetime.timezone.utc)
    # Calculate the time to the next leap second after the epoch.
    if index == len(dates) - 1:
        next_leap = float('inf')
    else:
        next_leap = (dates[index + 1] - dates[index]).total_seconds()
    # Return everything.
    return dt, leaps[index]['tai_utc'], leaps[index]['tai_utc'] - 10, \
        next_leap


def _get_parser():
    """ Get the command line argument parser.

    Returns
    -------
    parser : argparse.ArgumentParser
        The argument parser.

    See Also
    --------
    argparse.ArgumentParser

    """
    description = 'Log extractor and combiner for DSHIP data from ' \
        'research ships of the Leitstelle Deutsche Forschungsschiffe.'

    epilog = ''

    parser = argparse.ArgumentParser(description=description,
                                     epilog=epilog)

    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s ' + str(__version__))
    parser.add_argument('-o', '--output-file', type=str,
                        default='dship.nc',
                        help='The output file path. The default is '
                        'dship.nc.')
    parser.add_argument('-a', '--attr-file', type=str,
                        default=None,
                        help='The path to a JSON file containing the '
                        'Attributes to set in the NetCDF4 file.')
    parser.add_argument('-c', '--chunk-size', type=int,
                        default=100000,
                        help='The chunk length to use for storing the '
                        'variables. Must be at least 1000. Default is '
                        '100,000; which is a good balance between file '
                        'size reduction, read and write latency, not '
                        'having too many chunks, and the chunks '
                        'fitting in the chunk cache.')
    parser.add_argument('-q', '--quiet', action='store_true',
                        help='Don''t log information to stdout.')
    parser.add_argument('-l', '--log', action='store_true',
                        help='Log detailed information to a file with '
                        'the same name as OUTPUT-FILE but with the '
                        "extension changed to '.log'.")
    parser.add_argument('inputfiles', metavar='INPUTFILE', type=str,
                        nargs='*',
                        help='The DSHIP logs to process. ZIP and TAR '
                        'files are supported as well as gzipped and '
                        'bzip2ed data files.')

    return parser


def main(argv=None):
    """ Run this program with it connected to the command line.

    Parameters
    ----------
    argv : iterable of str, optional
        The input arguments to use. The default is ``sys.argv``.

    See Also
    --------
    sys.argv

    """
    # Grab the input arguments if none were given.
    if argv is None:
        argv = sys.argv

    # Parse the input arguments.
    parser = _get_parser()
    args = parser.parse_args(argv[1:])

    # Get the Attributes if an Attribute file was given.
    if args.attr_file is not None:
        with open(args.attr_file, 'rt') as f:
            attrs = json.load(f)
    else:
        attrs = dict()

    # Run.
    process_dship_logs(args.output_file, args.inputfiles,
                       chunksize=args.chunk_size,
                       log_to_stdout=(not args.quiet),
                       log_to_file=args.log, **attrs)


# Run main.
if __name__ == '__main__':
    sys.exit(main())
