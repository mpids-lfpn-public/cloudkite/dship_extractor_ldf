.. dship_extractor_ldf documentation master file, created by
   sphinx-quickstart on Wed Jul 29 10:04:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dship_extractor_ldf's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   information
   cli
   api



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
