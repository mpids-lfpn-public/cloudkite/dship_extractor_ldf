.. _cli-programs:


Command Line Interface
======================


.. autoprogram:: dship_extractor_ldf:_get_parser()
   :prog: python3 -m dship_extractor_ldf
