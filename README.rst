Overview
========

This utility takes one or more DSHIP logs frome one of the research ships of the
`Leitstelle Deutsche Forschungsschiffe <https://www.ldf.uni-hamburg.de>`_ (LDF),
extracts and combines the contents, and stores the results in a single
`NetCDF <https://www.unidata.ucar.edu/software/netcdf/>`_ file. Currently, many
quantities exported by the DSHIP software on the RV Meteor and RV Maria S. Merian
are supported.

This software was written by the Laboratory for Fluid Physics and Biocomplexity
(LFPB) of the Max-Planck-Insitut für Dynamik und Selbstorganisation (Max Planck
Institute for Dynamics and Self-Organization) and is provided under the MIT
license (see :file:`COPYING.txt`).


Dependencies
============

Only Python >= ``3.3`` is supported due to the use of some changes in the
:py:mod:`tar` and :py:mod:`zip` modules that occurred in that version that are
depended upon.

The following Python packages are required for operation

* `numpy <https://pypi.org/project/numpy>`_
* `scipy <https://pypi.org/project/scipy>`_ >= 0.18
* `astropy <https://pypi.org/project/astropy>`_ >= 2.0
* `netCDF4 <https://pypi.org/project/netCDF4>`_ >= 1.4

The following Python package is required for building

* `setuptools <https://pypi.org/project/setuptools>`_ >= 36.6
* `wheel <https://pypi.org/project/wheel>`_


Installation
============

The package's dependencies can be installed by::

    pip install -r requirements.txt

Then, to install the package, run either::

    pip install .

or, using the legacy :file:`setup.py` script::

    python setup.py install


Documentation
=============

The documentation requires the following packages

* `Sphinx <https://pypi.org/project/Sphinx>`_ >= 1.4
* `sphinx_rtd_theme <https://pypi.org/project/sphinx_rtd_theme>`_
* `sphinxcontrib-autoprogram <https://pypi.org/project/sphinxcontrib-autoprogram>`_

The required dependencies to build it can be installed from the main
directory with the command::

    pip install -r requirements_doc.txt

To build the HTML documentation, run either::

    sphinx-build doc/source doc/build/html

or, using the legacy :file:`setup.py` script::

    python setup.py build_sphinx

Documentation is put in the ``doc/build`` subdirectory. HTML
documentation is the default generated.

Running
=======

Command Line
------------

This package can be run from the command line in several different ways where
one or more DSHIP logs can be passed as well as other options controlling how
the data is stored. See :ref:`cli-programs` for a detailed description of the
command line arguments.

The main Python file can be executed as a standalone script by::

    ./dship_extractor_ldf.py [ARGS]

Or, if the module is in one's Python path, by::

    python3 -m dship_extractor_ldf [ARGS]

If this package is installed, a script is added to one's path that can be run from
the command line anywhere by::

    dship-extractor-ldf [ARGS]

The program prints its progress on standard output.


Examples
++++++++

Suppose we have the following log files in all five formats supported by this
package.

* :file:`dship_dgps1.dat` (data file extracted from the ZIP file exported by
  the DSHIP software)
* :file:`dship_gyro.dat.gz` (extracted data file then compressed with
  :command:`gzip`)
* :file:`dship_gyro.dat.bz2` (extracted data file then compressed with
  :command:`bzip2`)
* :file:`dship_weather_20200101-20200131.zip` (the DSHIP software exports ZIP
  files by default)
* :file:`dship_weather_20200201-20200229.tgz` (suppose one converted the
  exported ZIP file to a tarball at some point)

They could be processed by::

    ./dship_extractor_ldf.py \
       dship_dgps1.dat dship_gyro.dat.gz dship_gyro.dat.bz2 \
       dship_weather_20200101-20200131.zip \
       dship_weather_20200201-20200229.tgz

Which would store the results in the default file :file:`dship.nc`. If we want
to explicitly choose the name of the output file, we need to use the ``-o``
option. If we wanted to save to :file:`dship_all.nc`, we would pass
``-o dship_all.nc`` by::

    ./dship_extractor_ldf.py -o dship_all.nc \
       dship_dgps1.dat dship_gyro.dat.gz dship_gyro.dat.bz2 \
       dship_weather_20200101-20200131.zip \
       dship_weather_20200201-20200229.tgz

If we want to add global Attributes to the Dataset, we can put them in a JSON
file that we pass using the ``-a`` option. If we have stored the Attributes in
:file:`attributes.json`, we would pass ``-a attributes.json`` by::

     ./dship_extractor_ldf.py -a attributes.json \
       dship_dgps1.dat dship_gyro.dat.gz dship_gyro.dat.bz2 \
       dship_weather_20200101-20200131.zip \
       dship_weather_20200201-20200229.tgz

All Variables whose leading dimension is time are chunked with the same chunk
size. By default, this is ``100,000``; which is a good balance between file
size reduction, read and write latency, not having too many chunks, and keeping
the chunk size of most Variables small enough to fit into the default size of
the HDF5 chunck cache (1 MB). This size can be changed using the ``-c`` option.
If we wanted to change it to one million, we would pass ``-c 1000000`` by::

     ./dship_extractor_ldf.py -c 1000000 \
       dship_dgps1.dat dship_gyro.dat.gz dship_gyro.dat.bz2 \
       dship_weather_20200101-20200131.zip \
       dship_weather_20200201-20200229.tgz

If we want turn off the logging to standard out, pass the ``-q`` option.
Additionally, a much more detailed log can be written to a file with the same
name as the ouptut file but with the extension changed to ``log`` by passing
the ``-l`` (lower case L, not one) option.


Programmatically
----------------

All command line calls are parsed and turned into calls to
:py:func:`dship_extractor_ldf.process_dship_log`. One major difference is that
it does not have a default output filename and Attributes are passed as extra
keyword arguments rather than as a JSON file. If we take the previous example
and suppose the Attributes we wanted to set are ``campaign_id`` to ``'EUREC4A``
and ``platform_id`` to ``'MS-Merian'``, and want to log both to standard out
and file we would do::

    >>> import dship_extractor_ldf
    >>> dship_extractor_ldf.process_dship_logs(
    ...     'dship_all.nc',
    ...     ['dship_dgps1.dat',
    ...      'dship_gyro.dat.gz',
    ...      'dship_gyro.dat.bz2',
    ...      'dship_weather_20200101-20200131.zip',
    ...      'dship_weather_20200201-20200229.tgz'],
    ...     chunksize=1000000,
    ...     log_to_stdout=True,
    ...     log_to_file=True,
    ...     campaign_id='EUREC4A',
    ...     platform_id='MS-Merian')


Limitations
===========

This package has a number of limitations at the present time.

Missing Quantities
------------------

There are a number of DSHIP quantities that have not yet been matched to
what they mean and given Attributes, even for the RV Meteor and RV Maria S.
Merian for which this package was tested for.

Output Data Frequency
---------------------

The output file's data frequency is 1 Hz, even if all input files have a
frequency of once per minute.

Variables Filled Mostly with Missing Value for Once Per Minute Data
-------------------------------------------------------------------

If the DSHIP logs provide data once per minute, the resulting Variables in the
output file will all be filled with the ``missing_value`` for the respective
Variable except for one value each 60 seconds.

Primitive Combination of Values from Multiple Logs for The Same Quantity
------------------------------------------------------------------------

.. versionchanged:: 0.2

When multiple logs have the same quantity at the same time, their combination
is rather primitive. If it is an integer type and there is more than one
non-missing value for a single DSHIP source variable, they are averaged
together. If it is a floating point type, the one (or ones) with the
best/finest/smallest precision are averaged together.

If there is more than one source variable for a given quantity whose value is
not the missing value, they are combined int he same way unless there is a
single source variable whose name begins with ``'SYS.'`` in which case that
value is chosen whether it is the missing value or not, most precise or not.

Precisions
----------

.. versionadded:: 0.2

There are no DSHIP quantities explicitly giving the precisions. So the
precisons are heuristically determined by looking at the textrual
representations of the values. First, the precision is estimated from the
number of significant figures in the textual representation. This is a lower
bound on the precision because the DSHIP log exporter zero pads on the right
hand side to give a fixed number of digits after the decimal point. The
DSHIP export settings can adjust the number of digits and cause an excess number
to be used than the actual precision warrants. To determine this, the whole log
is parsed but with trailing zeros stripped from the right hand side of the
textual representations of numbers to get an estimate of the smallest precision
for each quantity. Then, the precision for each element is calculated to be the
maximum of this smallest value after removing trailing zeros and the precision
indicated by significant figures without removing trailing zeros.

Leap Seconds
------------

The package is leap second aware in that it chooses the zero point (the epoch)
of the time Variable to make sure no leap seconds are between the zero point and
the first data point. However, it cannot handle any leap seconds in the middle
of the data and raises a :py:exc:`RuntimeError` exception.


Version History
===============

0.4.1 (2021-11-02)
------------------

Bugfix release fixing a sign error in
``geoid_height_above_reference_ellipsoid``. The sign was not actually supposed
to be flipped.


0.4 (2021-09-23)
----------------

Changed ``geoidal_separation`` to ``geoid_height_above_reference_ellipsoid``
which is a standard name. Note that the sign is changed in the process. For
the grid mapping Variable, it is assumed that the sources giving data to the
DSHIP system use the WGS 84 ellipsoid, which is pretty standard but not
guaranteed to be correct.


0.3 (2021-09-15)
----------------

Quantities that don't have a single non-missing value are now excluded when
executing the script or running :py:func:`dship_extractor_ldf.process_dship_log`
directly.

The ``history`` Attribute in the Dataset is now set.

Added Dataset Attributes ``source_names_used``,
``source_names_with_no_non_missing_data``, and ``source_names_rejected`` which
are lists of the DSHIP source names used, rejected since they have no
non-missing data at all, and rejected since they are not in the information
lookups respectively. They are also logged.

Since a lot of DSHIP quantities are basically the same but from different
sources (e.g. ``CNAV1.GPVTG.SpeedK``, ``CNAV2.GPVTG.SpeedK``, and
``SEAPATH.INVTG.SpeedK``), the quantity lookup and determination now tries the
the source name, the source name with all characters before the first period
dropped, and the latter with that period and the first two capital letters
removed (this coalesces the three example quantities). Many quantities were
coalesced together, particularly those from NMEA 0183 sentences, thereby
allowing many more quantities to be successfully identified, especially GNSS
quantities.

Added support for the following additional quantities:

* ``*DBS.DepthFathoms``
* ``*DBS.DepthFeet``
* ``*DBS.DepthMeter``
* ``.SBE21.Conductivity``
* ``.SBE21.Conductivity_mS``
* ``.SBE21.Density``
* ``.SBE21.Salinity`` (treating as ``sea_water_salinity``, which could be the wrong salinity)
* ``.SBE21.SoundVeloExt``
* ``.SBE21.TempExtern`` (treating as ``sea_surface_temperature``, which could be wrong)
* ``NavLot.PSKPDPT.Offset_from_transducer``
* ``NavLot.PSKPDPT.Water_depth_relative``
* ``Parasound.PHDPT.offset_from_transducer_PHF``
* ``Parasound.PHDPT.water_depth_PHF``
* ``Parasound.PLDPT.offset_from_transducer_SLF_PLF``
* ``Parasound.PLDPT.water_depth_SLF_PLF``

Added support for the following additional units:

* ``F`` (fathom)
* ``S/m`` (siemen per meter)
* ``ft`` (foot)
* ``kHz``
* ``mS/cm`` (millisiemen per meter)


0.2.6 (2021-06-07)
------------------

Bugfix release. The following quantities mistakenly had ``valid_min`` Attribute
values of 0 even though they could have either sign. The Attribute has been
removed.

* ``platform_fore_velocity_wrt_ground``
* ``platform_fore_velocity_wrt_sea_water``
* ``platform_starboard_velocity_wrt_ground``
* ``platform_starboard_velocity_wrt_sea_water``

0.2.5 (2021-05-20)
------------------

Bugfix release fixing the following bug:

* Fixed the spelling of the ``difference_between_UTC_and_TAI_at_zero``
  Attribute on the ``time`` Variable. Previously, the 'w' in 'between' was
  missing.

0.2.4 (2021-05-12)
------------------

Bugfix release fixing the following bugs:

* Reverted ``Seapath.INGGA.Altitude`` back to giving ``alt`` instead of
  ``sea_surface_height_above_mean_sea_level``. It was set to this in version
  0.2, which was incorrect.

0.2.3 (2021-05-10)
------------------

Bugfix release fixing the following bugs:

* The best precision seen so far for each quantity after removing trailing zeros
  was not being set (was being skipped). This meant that doing two passes didn't
  do anything other than consume time.

0.2.2 (2021-04-29)
------------------

Bugfix release fixing the following bugs:

* ``str`` quantities were truncated to one character.


0.2.1 (2021-04-26)
------------------

Bugfix release fixing the following bugs:

* ``str`` quantities were replaced with the missing value if they were not
  exactly one character long.


0.2 (2021-04-23)
----------------

The build system was changed by moving all information in :file:`setup.py` over
to :file:`pyproject.toml` and :file:`setup.cfg` and now requires
`setuptools <https://pypi.org/project/setuptools>`_ >= 36.6 since :pep:`517` and
:pep:`518` support are now required. :file:`setup.py` has been reduced to the
very minimum required to still support use in the traditional fashion.

The logging was completely redone. Before, progress was printed to standard out.
That is still done by default (can be turned off) but the :py:mod:`logging`
module is used, more information is printed to standard out, and even more
detailed information can optionally be logged to a file with the same name as
the output file but with the extension changed to ``log``.

The method of combining values for the same quantity from multiple logs has been
changed from just blindly averaging valid values to averaging only those values
with the best precision (may just be a single value, or it could be more than
one with the same precision). The precision is determined from the number of
significant figures in the textual representation of each value and those of
all values with trailing zeros on the right hand sides removed. The precisions
are stored in Variables with the suffix ``'_precision_from_digits'``. This
requires two passes of each file and thus significantly increases the
computation time.

Added the ability to parse latitudes and longitudes in the form
``059° 37.748434' W``.

The computation of the ``actual_range`` Attribute for numerical quantities is
now calculated as the logs are iterated instead of at the very end by reading
back the whole arrays from disk. The new method minimized RAM usage and avoids
having to do large reads from the NetCDF4 file.

Fixed the following:

* ``Seapath.INGGA.Altitude`` gives ``sea_surface_height_above_mean_sea_level``,
  not ``alt`` like before.


0.1.4 (2021-02-12)
------------------

Paths can now be given as directories, which are searched recursively for
potential DSHIP log files based on their file extensions (only ``'.zip'``,
``'.tgz'``, ``'.tbz'`` and ``'.dat'`` or ``'.tar'`` without compression
extension or with the compression extensions ``'.gz'`` or ``'.bz2'``).

Fixed the following bugs:

* Couldn't properly identify the ``errorvaluealphanumeric`` property in the
  order XML files for getting the string used for error/invalid values.


0.1.3 (2021-02-11)
------------------

Bugfix release fixing the following bugs:

* Not properly flagging invalid/error values with non-standard error values, but
  are specified in the order XML file in ZIP and TAR exports. The order XML file
  is now minimally parsed to get the invalid/error field values.
* dtype of ``Seapath.PSXN.ID`` changed to int32 from int8 to match the DSHIP
  documentation.
* ``Weatherstation.PEUMA.Air_pressure2`` was incorrectly considered to be
  measuring ``air_pressure`` when it is actually
  ``air_pressure_at_mean_sea_level``.


0.1.2 (2021-02-10)
------------------

Bugfix release properly enabling compression for the data.


0.1.1 (2021-02-10)
------------------

Added the global Attribute ``software_versions`` that contains the versions of
all packages used as well as the Python implementation and version. It is
a string with ``'\n'`` (Unix newlines)separating each package/python. Each line
is formatted as the package/python name, a colon and a space, and the version in
double quotes (or, in the case of python, the implementation and the version
with a space between them). An example is::

    dship_extractor_ldf: "0.1.1"
    numpy: "1.20.0"
    cipy: "1.6.0"
    astropy: "4.2"
    netCDF4: "1.5.5.1"
    python: "CPython 3.9.1"

Added support for the following additional DSHIP source names.

* ``Seapath.INGGA.Age``
* ``Seapath.INGGA.Altitude``
* ``Seapath.INGGA.Dilution``
* ``Seapath.INGGA.Latitude``
* ``Seapath.INGGA.Longitude``
* ``Seapath.INGGA.Quality``
* ``Seapath.INGGA.Separation``
* ``Seapath.INGGA.Satellites``
* ``Seapath.INGGA.Station_ID``
* ``Seapath.PSXN.ID``
* ``Seapath.INVTG.CourseM``
* ``Seapath.INVTG.CourseT``
* ``Seapath.INVTG.SpeedK``
* ``Seapath.INVTG.SpeedN``


0.1 (2020-07-29)
----------------

Initial version.
